import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FullComponent } from './layouts/full/full.component';
import { BlankComponent } from './layouts/blank/blank.component';
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./website/home-page/home-page.module').then(m => m.HomePageModule)
  },
  {
    path: 'privacy',
    loadChildren: () => import('./website/privacy/privacy.module').then(m => m.PrivacyModule)
  },
  {
    path: 'terms-of-use',
    loadChildren: () => import('./website/terms-of-use/terms-of-use.module').then(m => m.TermsofUseModule)
  },
  {
  path: '',
  component: FullComponent,
  children: [
    // { path: '', redirectTo: '/starter', pathMatch: 'full' },
    {
      path: '',
      loadChildren: () => import('./user/user.module').then(m => m.UserModule),
      data: {
        role: 'user'
      },
    },
    {
      path: 'admin',
      loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
      data: {
        role: 'admin',
      }
    }
  ]
},
{
  path: '',
  component: BlankComponent,
  children: [
    {
      path: 'authentication',
      loadChildren:
        () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
    }
  ]
},
{
  path: '**',
  redirectTo: 'authentication/404'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
