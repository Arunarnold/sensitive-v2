import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from "../../shared/global-service.service";
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {
  setMenuRole:string;
  color = 'blue';
  showSettings = false;
  showMinisidebar = false;
  showDarktheme = false;
  showRtl = false;
  
  public innerWidth: any;

  public config: PerfectScrollbarConfigInterface = {};

  constructor(private service: GlobalServiceService, public router: Router) {
    service.currentMessage.subscribe(message=>{
      if(message == "admin"){
        this.setMenuRole = message;
      }else if(message == "user"){
        this.setMenuRole = message;
      }
    })
  }

  ngOnInit() {
    if(localStorage.getItem('loginID') && localStorage.getItem('token') &&  JSON.parse(localStorage.getItem('loginID')).role ){
      let userData = JSON.parse(localStorage.getItem('loginID'));
      let role = userData.role;
      console.log(role);
      // this.obeservableTimer();
      if(role == 'admin'){
      this.router.navigate(['/admin/dashboard']);
      this.setMenuRole = 'admin';
      }else if(role == 'user'){
      this.router.navigate(['/dashboard_user']);
      this.setMenuRole = 'user';
      }
    }else{
      localStorage.removeItem('loginID');
      localStorage.removeItem('loggedIntime');
      let dataMessage = {
        message : "loginID",
        token: ''
      }
      // this.service.changeMessage(dataMessage);
      this.router.navigate(['authentication/login'],{ replaceUrl: true });
    }
    let body = document.body;
    body.classList.add("addDashboardTheme");
    this.handleLayout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.handleLayout();
  }

  toggleSidebar() {
    this.showMinisidebar = !this.showMinisidebar;
  }

  handleLayout() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 1170) {
      this.showMinisidebar = true;
    } else {
      this.showMinisidebar = false;
    }
  }
}
