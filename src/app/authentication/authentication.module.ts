import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';


import { NotfoundComponent } from './404/not-found.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AuthenticationRoutes } from './authentication.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { WebsiteModule } from '../website/website/website.module'
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthenticationRoutes), FormsModule, ReactiveFormsModule,WebsiteModule
  ],
  declarations: [
    NotfoundComponent,
    LoginComponent,
    SignupComponent,
    ForgotPasswordComponent
  ]
})
export class AuthenticationModule {}
