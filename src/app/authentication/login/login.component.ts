import { Component, OnInit, AfterViewInit, ViewChild, OnDestroy, ElementRef, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GlobalServiceService } from '../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
  constructor(public router: Router, private formBuilder: FormBuilder,
    private service: GlobalServiceService, public toastr: ToastrService) { }
  // DECLARATIONS
  // ====================
  form: FormGroup;
  login_url = 'user_table';
  users_list: any;
  validate;
  value;
  user_log_url = "user_log";
  @ViewChild('recaptcha', { static: true }) recaptchaElement: ElementRef;
  // VALIDATION MESSAGES
  // ============================
  validation_messages = {
    'user_name': [
      { type: 'required', message: 'User Name is required.' }
    ],
    'user_password': [
      { type: 'required', message: 'Password is required.' }
    ]
  };
  // ===================================================================================================================
  ngOnInit() {
    window.scroll(0, 0);
    this.onLoginFormInit();
    this.onGetUsersList();
  }
  // ==========================================================================================================================
  // GET USER LIST
  // ================
  onGetUsersList() {
    this.service.get_lists(this.login_url).subscribe(res => {
      this.users_list = res;
    });
  }
  // =========================================================================================================================
  // FORM INITIATE
  // ========================
  onLoginFormInit() {
    this.form = this.formBuilder.group({
      user_name: new FormControl(''),
      user_password: new FormControl('')
    });
  }
  // ==============================================================================================================================
  ngAfterViewInit() {
    // if (this.form.get('captcha').value) {
    //   this.value = this.form.get('captcha').value;
    // }
  }
  // ==============================================================================================================
  // LOGIN METHOD
  // =====================
  onLoggedin() {
    this.setValidators(this.form);

    if (this.form.valid) {

      if(this.form.value.user_name == 'user@lion.com' && this.form.value.user_password == '123456'){
        const loginId = {
          role: "user"
        };
        const token = "123456";
        localStorage.setItem('loginID', JSON.stringify(loginId));
        localStorage.setItem('token', JSON.stringify(token));
        this.router.navigate(['/admin/dashboard']);
      }else if(this.form.value.user_name == 'admin@lion.com'  && this.form.value.user_password == '123456'){
        const loginId = {
          role: "admin"
        }
        const token = "123456";
        localStorage.setItem('loginID', JSON.stringify(loginId));
        localStorage.setItem('token', JSON.stringify(token));
        this.router.navigate(['/dashboard_user']);
      }


      // if (user_data) {
      //   if (user_data.password === pwd) {
      //     const obj = {
      //       user_id: user_data.id,
      //       name: user_data.name,
      //       login_time: new Date(),
      //       logout_time: null,
      //       user_email: user_data.email_id,
      //       user_type: user_data.user_type,
      //       role_id: user_data.role_id
      //     };
      //     this.service.post_data(this.user_log_url, obj).subscribe((result) => {
      //       this.toastr.success('Login Successfully');
      //       localStorage.setItem('user_id', user_data.id);
      //       localStorage.setItem('user_log_id', result.id);
      //       this.service.Emitting(0);
      //       if (user_data.role_id == 1) {
      //         this.router.navigate(['/dashboard_admin']);
      //       }
      //       if (user_data.role_id == 2) {
      //         this.router.navigate(['/dashboard_personal']);
      //       }
      //       if (user_data.role_id == 3) {
      //         this.router.navigate(['/dashboard_business']);
      //       }
      //     });
      //   } else {
      //     this.toastr.error('Password Incorrect');
      //   }
      // } else {
      //   this.toastr.error('Invalid User');
      // }
    } else {
      this.validateAllFormFields(this.form);
      this.form.get('user_name').clearValidators();
      this.form.get('user_password').clearValidators();
    }
  }
  setValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      formGroup.get(field).setValidators(Validators.required);
      formGroup.get(field).updateValueAndValidity();
    });
  }
  removeValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      formGroup.get(field).clearValidators();
      formGroup.get(field).updateValueAndValidity();
    });
  }
  // ===================================================================================================
  //  VALIDATION METHOD
  // ========================
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsPending({ onlySelf: true })
        // control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  // =====================================================================================================
  ngOnDestroy() {
    // const x = document.getElementById('cap') as HTMLElement;
    // x.remove();
    // const captchaElem = this.reCaptcha['elementRef'].nativeElement;
    // captchaElem.parentElement.removeChild(captchaElem);
  }
  // renderReCaptch() {

  //   window['grecaptcha'].render(this.recaptchaElement.nativeElement, {
  //     'sitekey': '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  //     'callback': (response) => {
  //       this.form.get('captcha').patchValue(response);
  //     }
  //   });
  // }

  // addRecaptchaScript() {
  //   window['grecaptchaCallback'] = () => {
  //     // this.renderReCaptch();
  //     window['grecaptcha'].render(this.recaptchaElement.nativeElement, {
  //       'sitekey': '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  //       'callback': (response) => {
  //         this.form.get('captcha').patchValue(response);
  //       }
  //     });
  //   }
  //   (function (d, s, id, obj) {
  //     var js, fjs = d.getElementsByTagName(s)[0];
  //     if (d.getElementById(id)) { obj.renderReCaptch(); return; }
  //     js = d.createElement(s); js.id = id;
  //     js.src = "https://www.google.com/recaptcha/api.js?onload=grecaptchaCallback&amp;render=explicit";

  //     fjs.parentNode.insertBefore(js, fjs);
  //   }(document, 'script', 'recaptcha-jssdk', this));
  // }
}
