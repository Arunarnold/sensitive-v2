import { Component, OnInit, OnDestroy, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { GlobalServiceService } from '../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SignupComponent implements OnInit, OnDestroy {
  constructor(private service: GlobalServiceService, private router: Router,
    private FB: FormBuilder, public toastr: ToastrService, private route: ActivatedRoute) {

  }

  // DECLARATIONS
  // ======================
  signupForm: FormGroup;
  user_url = 'user_table';
  user_id: any;
  validate: any;
  signature_design_details_url = 'signature_details';
  plans_detail_url = "plans_detail";
  user_type: any;
  title: any;
  product: any;
  @ViewChild('recaptcha', { static: true }) recaptchaElement: ElementRef;
  // VALIDATION MESSAGES
  // ============================
  validation_messages = {
    'name': [
      { type: 'required', message: 'Name is required.' }
    ],
    'email_id': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Email not Valid' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'pattern', message: 'Password containing at least 8 characters, 1 number,1 special character, 1 upper and 1 lowercase.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm Password is required.' },
      {
        type: 'pattern',
        message: 'Confirm Password containing at least 8 characters, 1 number,1 special character, 1 upper and 1 lowercase.'
      }
    ],
    'agree_terms': [
      { type: 'required', message: 'Please tick the Terms' }
    ]
  };
  ngOnInit() {
    window.scroll(0, 0);
    this.route.paramMap.subscribe(params => {
      this.user_type = params.get('id');
      if (this.user_type == 2) {
        this.title = 'Personal';
      } else {
        this.title = 'Business';
      }
    });
    this.FormInit();
  }
  // ===================================================================================================
  // FORM INITIATE
  // ====================
  // Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)
  FormInit() {
    this.signupForm = this.FB.group({
      name: new FormControl(''),
      email_id: new FormControl(''),
      password: new FormControl(''),
      confirm_password: new FormControl(''),
      agree_terms: new FormControl(''),
      role_id: new FormControl(Number(this.user_type)),
      user_type: new FormControl(Number(this.user_type)),
      pro_id: new FormControl(0),
      status_id: new FormControl(1)
    });
  }
  // ===================================================================================================
  //  VALIDATION METHOD
  // ========================
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsPending({ onlySelf: true });
        // control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  isFieldValid(field: string) {
    return !this.signupForm.get(field).valid && this.signupForm.get(field).touched;
  }
  isFileValid(field: string) {
    return !this.signupForm.get(field).valid;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }
  setValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      if (field == 'email_id') {
        formGroup.get(field).setValidators([Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]);
      } else
        if (field == 'password' || field == 'confirm_password') {
          formGroup.get(field).setValidators([Validators.required,
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,20}$/)]);
        }
        else {
          formGroup.get(field).setValidators(Validators.required);
        }
      formGroup.get(field).updateValueAndValidity();
    });
  }
  // =================================================================================================================
  // SIGN UP
  // =====================
  signUp() {
    if (this.signupForm.get('agree_terms').value == false) {
      this.signupForm.get('agree_terms').patchValue('');
    }
    this.setValidators(this.signupForm)
    if (this.signupForm.valid) {
      // if (this.validate == 'success') {
      if (this.signupForm.get('password').value == this.signupForm.get('confirm_password').value) {
        this.signupForm.removeControl('confirm_password');
        this.signupForm.removeControl('agree_terms');
        this.signupForm.value.register_date = new Date();
        this.service.post_data(this.user_url, this.signupForm.value).subscribe((result) => {
          // if (this.user_type == 2) {
          //   const sign = {
          //     'user_id': result.id,
          //     'signature_name': result.name + ' Signature',
          //     'design_id': null,
          //     'name': null,
          //     'title': null,
          //     'company': null,
          //     'mobile': null,
          //     'website': null,
          //     'email': null,
          //     'link_to_map': false,
          //     'address': null,
          //     'address1': null,
          //     'city': null,
          //     'state': null,
          //     'postal_code': null,
          //     'image_in_binary': null,
          //     'image': null,
          //     'image_url': null,
          //     'dynamic_details': [],
          //     'check_disclaimer': false,
          //     'check_banner': false,
          //     'check_images': false,
          //     'check_socials': false,
          //     'check_youtube': false,
          //     'check_branding_status': false,
          //     'check_sales_events': false,
          //     "check_appstore": false,
          //     'dynamic_social_icons': [],
          //     'signature_styles': null,
          //     'dynamic_cta_details': {
          //       'dynamic_cta_order': [],
          //       'dynamic_disclaimer_details': {
          //         'disclaimer_value': null,
          //         'disclaimer_style': null
          //       },
          //       'dynamic_images_details': {
          //         'heading': null,
          //         'images': null,
          //         'image_size': null
          //       },
          //       'dynamic_banner_details': {
          //         'banner_image': null,
          //         'banner_size': null,
          //         'banner_url': null
          //       },
          //       'dynamic_sales_event_details': {
          //         'title': null,
          //         'link_text': null,
          //         'link_url': null,
          //         'selected_icon': null
          //       },
          //       'dynamic_social_icons': {
          //         'badge': [],
          //         'button_style': 'full'
          //       },
          //       'dynamic_youtube_video': {
          //         'url': null,
          //         'title': null,
          //         'video_style': null,
          //         'description': null,
          //         'image': null
          //       },
          //       "dynamic_app_store": {
          //         "playstore_link": null,
          //         "applestore_link": null
          //       }
          //     },
          //     'signature_font_color': {
          //       'font_size': null,
          //       'font_family': 'Verdana',
          //       'font_color': '#000'
          //     },
          //     'logo_style': {
          //       'logo_shape': null,
          //       'logo_size': null
          //     },
          //     'social_icons_style': {
          //       'icon_shape': null,
          //       'icon_size': null,
          //       'is_match_template_color': null
          //     }
          //   };
          //   const plan = {
          //     "user_id": result.id,
          //     "plan_id": 1,
          //     "upgrade_date": new Date(),
          //     "payement_details": {
          //       "email_id": this.signupForm.get('email_id').value,
          //       "card_number": null,
          //       "month": null,
          //       "year": null,
          //       "cvc": null
          //     },
          //     "status": 1
          //   }
          //   this.service.post_data(this.signature_design_details_url, sign).subscribe((res) => {
          //   });
          //   this.service.post_data(this.plans_detail_url, plan).subscribe((res) => {
          //   });
          // }
          this.toastr.success('Registered Successfully');
          this.router.navigate(['/authentication/login']);
        });
        this.signupForm.addControl('confirm_password', new FormControl(null));
        this.signupForm.addControl('agree_terms', new FormControl(null));
      } else {
        this.toastr.error('Password and Confirm Password Not Match');
      }
      // } else {
      //   this.toastr.error('Captcha Invalid');
      //   this.signupForm.get('captcha').reset();
      // }
    } else {
      this.validateAllFormFields(this.signupForm);
      this.signupForm.get('name').clearValidators();
      this.signupForm.get('email_id').clearValidators();
      this.signupForm.get('password').clearValidators();
      this.signupForm.get('confirm_password').clearValidators();
      this.signupForm.get('agree_terms').clearValidators();
      this.signupForm.get('role_id').clearValidators();
      this.signupForm.get('user_type').clearValidators();
      this.signupForm.get('name').clearValidators();
    }
  }


  // ===================================================================================================
  ngOnDestroy() {
    const x = document.getElementById('cap1') as HTMLElement;
    x.remove();
  }

  // renderReCaptch() {

  //   window['grecaptcha'].render(this.recaptchaElement.nativeElement, {
  //     'sitekey': '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  //     'callback': (response) => {
  //       // console.log(response);
  //       this.signupForm.get('captcha').patchValue(response);
  //     }
  //   });
  // }

  // addRecaptchaScript() {
  //   // console.log('test')
  //   window['grecaptchaCallback'] = () => {
  //     // this.renderReCaptch();
  //     window['grecaptcha'].render(this.recaptchaElement.nativeElement, {
  //       'sitekey': '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  //       'callback': (response) => {
  //         // console.log(response);
  //         this.signupForm.get('captcha').patchValue(response);
  //       }
  //     });
  //   }
  //   (function (d, s, id, obj) {
  //     var js, fjs = d.getElementsByTagName(s)[0];
  //     if (d.getElementById(id)) { obj.renderReCaptch(); return; }
  //     js = d.createElement(s); js.id = id;
  //     js.src = "https://www.google.com/recaptcha/api.js?onload=grecaptchaCallback&amp;render=explicit";

  //     fjs.parentNode.insertBefore(js, fjs);
  //   }(document, 'script', 'recaptcha-jssdk', this));
  // }
}
