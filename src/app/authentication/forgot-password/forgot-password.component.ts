import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { GlobalServiceService } from '../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ForgotPasswordComponent implements OnInit {
  form: FormGroup;
  OtpForm: FormGroup;
  resetForm: FormGroup;
  show = 1;
  otp: any;
  userUrl = 'user_table';
  userList = [];
  user: any;
  // VALIDATION MESSAGES
  // ============================
  validation_messages = {
    'user_name': [
      { type: 'required', message: 'User Name is required.' }
    ],
    'otp': [
      { type: 'required', message: 'OTP is required.' }
    ],
    'new_password': [
      { type: 'required', message: 'New Password is required.' },
      { type: 'pattern', message: 'Password containing at least 8 characters, 1 number,1 special character, 1 upper and 1 lowercase.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm Password is required.' },
      { type: 'pattern', message: 'Password containing at least 8 characters, 1 number,1 special character, 1 upper and 1 lowercase.' }
    ]
  };
  constructor(public Fb: FormBuilder, public service: GlobalServiceService, public toastr: ToastrService, public route: Router) { }
  ngOnInit() {
    this.formInit();
    this.getUsers();
  }
  // =======================================================================================================================
  // FORM INITIATE
  // =========================
  formInit = () => {
    this.form = this.Fb.group({
      user_name: new FormControl(null)
    });
    this.OtpForm = this.Fb.group({
      otp: new FormControl(null)
    });
    this.resetForm = this.Fb.group({
      new_password: new FormControl(null),
      confirm_password: new FormControl(null)
    });
  }
  // =======================================================================================================================
  // GET USERS
  // =================
  getUsers = () => {
    this.service.get_lists(this.userUrl).subscribe((result) => {
      this.userList = result;
    });
  }
  // =======================================================================================================================
  // GET OTP
  // ==============
  getOtp = () => {
    this.setValidators(this.form);
    if (this.form.valid) {
      this.user = this.userList.filter(ele => ele.email_id == this.form.value.user_name)[0];
      if (this.user) {
        this.otp = Math.floor(1000 + Math.random() * 9000);
        this.toastr.success('OTP Generated. The OTP Number is ' + this.otp);
        this.show = 2;
      } else {
        this.toastr.error('Invalid User');
      }
    } else {
      this.validateAllFormFields(this.form);
      this.form.get('user_name').clearValidators();
    }
  }
  // ======================================================================================================================
  // VERIFY OTP
  // =================
  verify = () => {
    this.setValidators(this.OtpForm);
    if (this.OtpForm.valid) {
      if (this.otp == this.OtpForm.value.otp) {
        this.toastr.success('OTP Verified Successfully');
        this.show = 3;
      } else {
        this.toastr.error('Please Enter Correct OTP (or) Regenerate the OTP');
      }
    } else {
      this.validateAllFormFields(this.OtpForm);
      this.OtpForm.get('otp').clearValidators();
    }
  }
  // ========================================================================================================================
  // RESET THE PASSWORD
  // =============================
  changePassword = () => {
    this.setValidators(this.resetForm);
    if (this.resetForm.valid) {
      if (this.resetForm.value.new_password == this.resetForm.value.confirm_password) {
        this.user.password = this.resetForm.value.new_password;
        this.service.put_data(this.userUrl, this.user.id, this.user).subscribe((result) => {
          this.toastr.success('Password Changed Successfully');
          this.route.navigate(['/authentication/login']);
        });
      } else {
        this.toastr.error('Password Mismatched');
      }
    } else {
      this.validateAllFormFields(this.resetForm);
      this.resetForm.get('new_password').clearValidators();
      this.resetForm.get('confirm_password').clearValidators();
    }
  }
  // ===========================================================================================================================
  //  VALIDATION METHOD
  // ========================
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        // control.markAsTouched({ onlySelf: true });
        control.markAsPending({ onlySelf: true })
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  setValidators(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      if (field == 'new_password' || field == 'confirm_password') {
        formGroup.get(field).setValidators([Validators.required,
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,20}$/)]);
      }
      else {
        formGroup.get(field).setValidators(Validators.required);
      }
      formGroup.get(field).updateValueAndValidity();
    });
  }
  // ============================================================================================================================
}
