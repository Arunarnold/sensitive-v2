import { Component, OnInit, TemplateRef  } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GlobalServiceService } from '../../shared/global-service.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
declare var require: any;
const data: any = require('./plan-details.json');
@Component({
  selector: 'app-plan-details',
  templateUrl: './plan-details.component.html',
  styleUrls: ['./plan-details.component.scss']
})
export class PlanDetailsComponent implements OnInit {

  modalRef: BsModalRef;
  planLists:[]= data;
  activPlanIndex = 0;
  constructor(private modalService: BsModalService, public fB: FormBuilder, private service: GlobalServiceService) {
  }

  ngOnInit() {
    this.formInit();
    this.getPlans();
  }
  // Declaration
  // ============================
  form: FormGroup;
  closeResult;
  planUrl = "plan";
  paymentUrl = "payment";
  planList = [];
  lastPayment = new payment();
  current_plan_id;

  // ============================================================================================
  // FormInit
  // ============================
  formInit = () => {
    this.form = this.fB.group({
      id: new FormControl(null),
      plan_id: new FormControl(null),
      payment_date: new FormControl(new Date()),
      end_date: new FormControl(null),
      name_on_card: new FormControl(null, Validators.required),
      card_number: new FormControl(null, [Validators.required, Validators.maxLength(12), Validators.minLength(12)]),
      month: new FormControl(null, [Validators.required, Validators.maxLength(2), Validators.minLength(2)]),
      year: new FormControl(null, [Validators.required, Validators.maxLength(2), Validators.minLength(2)]),
      cvc: new FormControl(null, [Validators.required, Validators.maxLength(3), Validators.minLength(3)]),
      status_id: new FormControl(1)
    });
  }
  // ============================================================================================
  // VALIDATION MESSAGES
  // ==========================================
  validation_messages = {
    'name_on_card': [
      { type: 'required', message: 'Name on card is required.' },
    ],
    'card_number': [
      { type: 'required', message: 'Card Number is required.' },
      { type: 'maxlength', message: '12 Numbers is required.' },
      { type: 'minlength', message: '12 Numbers is required.' }
    ],
    'month': [
      { type: 'required', message: 'Month is required.' },
      { type: 'maxlength', message: '2 Numbers is required.' },
      { type: 'minlength', message: '2 Numbers is required.' }
    ],
    'year': [
      { type: 'required', message: 'Year is required.' },
      { type: 'maxlength', message: '2 Numbers is required.' },
      { type: 'minlength', message: '2 Numbers is required.' }
    ],
    'cvc': [
      { type: 'required', message: 'CVC is required.' },
      { type: 'maxlength', message: '3 Numbers is required.' },
      { type: 'minlength', message: '3 Numbers is required.' }
    ]
  };
  // =====================================================================================================
  openPayment(event, template: TemplateRef<any>){
    event.stopPropagation();
    this.modalRef = this.modalService.show(template, {class: 'modal-lg'});
  }
  popUp(content) {
    // this.modalref1 = this.modalService
    //   .open(content, { ariaLabelledBy: 'modal-basic-title' });
    // this.modalref1.result.then(
    //   result => {
    //     this.closeResult = `Closed with: ${result}`;
    //   },
    //   reason => {
    //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //   }
    // );
  }

  // Get Functions
  // ==========================
  getPlans() {
    this.service.get_lists(this.planUrl).subscribe((result) => {
      this.planList = [];
      result.forEach((ele, i) => {
        if (ele.status_type == 1) {
          this.planList.push(ele);
        }
      })
      // console.log(this.planList)
      this.getCurrentPlan();
    })
  }
  getCurrentPlan() {
    // this.lastPayment = new payment();
    // this.service.get_lists(this.paymentUrl).subscribe((result) => {
    //   this.lastPayment = result.filter((f) => f.user_id == this.user_id).reverse()[0];
    //   if (this.lastPayment != undefined) {
    //     this.current_plan_id = this.lastPayment.plan_id;
    //   }
    //   // console.log(this.current_plan_id);
    // })
  }
  // ====================================================================== 

  // Crud
  // ===============================
  select_plan(plan_id, content) {
    this.form.get('plan_id').patchValue(plan_id);
    this.popUp(content);
  }

  pay() {
    if (this.form.valid) {
      this.service.post_data(this.paymentUrl, this.form.value).subscribe((result) => {
        if (this.lastPayment != undefined) {
          this.lastPayment.status_id = 2;
          this.lastPayment.end_date = new Date();
          this.service.put_data(this.paymentUrl, this.lastPayment.id, this.lastPayment).subscribe((result) => {
            // console.log(result)
          })
        }
        // this.modalref1.close();
        this.reset();
        this.getCurrentPlan();
      })
    }
  }
  reset() {
    this.form.reset();
    this.form.patchValue({
      payment_date: new Date()
    })
  }
  // ==========================================================================
}
export class payment {
  id: any;
  plan_id: any;
  user_id: any;
  payment_date: any;
  end_date: any;
  name_on_card: any;
  card_number: any;
  month: any;
  year: any;
  cvc: any;
  status_id: any;
}

