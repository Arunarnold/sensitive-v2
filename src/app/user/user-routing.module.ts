import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//user pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { wordListComponent } from './word-list/word-list.component';
import { FilterRulesComponent } from './filter-rules/filter-rules.component';
import { PlanDetailsComponent } from './plan-details/plan-details.component';
import { TestConsoleComponent } from './test-console/test-console.component';
import { BillingComponent } from './billing/billing.component';
import { SupportComponent } from './support/support.component';
import { SettingsComponent } from './settings/settings.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard_user', pathMatch: 'full' },
  {
    path:'dashboard_user',
    component:DashboardComponent,
    data: {
      title: 'Dashboard',
      urls: [
        { title: 'Home', url: '/dashboard_user' },
        { title: 'Dashboard' },
      ],
      role: 'user'
    }
  },
  {
    path:'word-list',
    component:wordListComponent,
    data: {
      title: 'Word List',
      urls: [
        { title: 'Home', url: '/dashboard_user' },
        { title: 'Word List' },
      ],
      role: 'user'
    }
  },
  {
    path:'filter-rules',
    component:FilterRulesComponent,
    data: {
      title: 'Filter Rules',
      urls: [
        { title: 'Home', url: '/dashboard_user' },
        { title: 'Filter Rules' },
      ],
      role: 'user'
    }
  },
  {
    path:'plan-details',
    component:PlanDetailsComponent,
    data: {
      title: 'Plan Details',
      urls: [
        { title: 'Home', url: '/dashboard_user' },
        { title: 'Plan Details' },
      ],
      role: 'user'
    }
  },
  {
    path:'test-console',
    component:TestConsoleComponent,
    data: {
      title: 'Test Console',
      urls: [
        { title: 'Home', url: '/dashboard_user' },
        { title: 'Test Console' },
      ],
      role: 'user'
    }
  },
  {
    path:'billing',
    component:BillingComponent,
    data: {
      title: 'Billing',
      urls: [
        { title: 'Home', url: '/dashboard_user' },
        { title: 'Billing' },
      ],
      role: 'user'
    }
  },
  // {
  //   path:'support',
  //   component:SupportComponent,
  //   data: {
  //     title: 'Support',
  //     urls: [
  //       { title: 'Home', url: '/dashboard_user' },
  //       { title: 'Support' },
  //     ],
  //     role: 'user'
  //   }
  // },
  {
    path:'settings',
    component:SettingsComponent,
    data: {
      title: 'Settings',
      urls: [
        { title: 'Home', url: '/dashboard_user' },
        { title: 'Settings' },
      ],
      role: 'user'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
