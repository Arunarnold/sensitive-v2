import { Component, AfterViewInit, ViewChild, ViewEncapsulation } from '@angular/core';

declare var require: any;
const data: any = require('./dashboard.json');
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements AfterViewInit {
  rows:any = [];
  temp:any = [];
  loginList = [];
  selectPageValue: number = 10;
  @ViewChild(DashboardComponent, {static: true}) table: DashboardComponent | undefined;
  constructor(){
    this.loginList = data;
    this.rows = this.loginList;
    this.temp = [...this.rows];
  }
  ngAfterViewInit(){

  }

    // This is for the dashboar line chart
  // lineChart
  public lineChartData: Array<any> = [
    { data: [50, 130, 80, 70, 180, 105, 250],label: 'Profanity', lineTension: 0},
    { data: [80, 100, 60, 200, 150, 100, 150],label: 'Total API Calls', lineTension:0}
  ];

  public lineChartLabels: Array<any> = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July'
  ];
  public lineChartOptions: any = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          },
          gridLines: {
            color: 'rgba(120, 130, 140, 0.13)'
          }
        }
      ],
      xAxes: [
        {
          gridLines: {
            color: 'rgba(120, 130, 140, 0.13)'
          }
        }
      ]
    },
    lineTension: 0,
    responsive: true,
    maintainAspectRatio: false
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: 'rgba(25,118,210,0.0)',
      borderColor: 'rgba(25,118,210,1)',
      pointBackgroundColor: 'rgba(25,118,210,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(25,118,210,0.5)'
    },
    {
      // dark grey
      backgroundColor: 'rgba(38,218,210,0.0)',
      borderColor: 'rgba(38,218,210,1)',
      pointBackgroundColor: 'rgba(38,218,210,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(38,218,210,0.5)'
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';



  //search filter
  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d:any) {
      return d.ipAddress.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.loginList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table = this.rows;
  }
}
