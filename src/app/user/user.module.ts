import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';

// user pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { wordListComponent } from './word-list/word-list.component';
import { FilterRulesComponent } from './filter-rules/filter-rules.component';
import { PlanDetailsComponent } from './plan-details/plan-details.component';
import { TestConsoleComponent } from './test-console/test-console.component';
import { BillingComponent } from './billing/billing.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { ToastrModule } from 'ngx-toastr';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { NgxDatatableModule } from '@swimlane/ngx-datatable';
// import { ChartistModule } from 'ng-chartist';
// import { DndModule } from 'ngx-drag-drop';
// import { ColorPickerModule } from 'ngx-color-picker';
import { SafePipe } from '../shared/safe.pipe';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { UiSwitchModule } from 'ngx-ui-switch';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 1,
  wheelPropagation: true,
  minScrollbarLength: 20
};
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SupportComponent } from './support/support.component';
import { SettingsComponent } from './settings/settings.component';
@NgModule({
  declarations: [DashboardComponent,wordListComponent,FilterRulesComponent,PlanDetailsComponent,TestConsoleComponent,BillingComponent,SupportComponent,SettingsComponent,SafePipe],
  imports: [
    CommonModule,
    UserRoutingModule,FormsModule, ReactiveFormsModule, PerfectScrollbarModule, NgxDatatableModule ,ChartsModule,UiSwitchModule,   TabsModule.forRoot(), CollapseModule.forRoot(), ModalModule.forRoot(), AccordionModule.forRoot(), BsDatepickerModule.forRoot()
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  },
  ]
})
export class UserModule { }
