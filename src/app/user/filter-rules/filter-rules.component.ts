import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl,Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-filter-rules',
  templateUrl: './filter-rules.component.html',
  styleUrls: ['./filter-rules.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class FilterRulesComponent implements OnInit {

  filterRulesForm:FormGroup;
  update_btn = false;
  constructor(public fb: FormBuilder, private toastr: ToastrService) {}
  ngOnInit() {
    this.filterRulesFormInit();
  }

  filterRulesFormInit = () => {
    this.filterRulesForm = this.fb.group({
      id: new FormControl(null),
      filter_urls: new FormControl(false),
      filter_phone_numbers: new FormControl(false),
      filter_emails: new FormControl(false),
      deep_search_ban: new FormControl(false),
      deep_search_allow: new FormControl(false),
      language_english : new FormControl(false),
      language_french : new FormControl(false),
      language_spanish: new FormControl(false),
      language_german: new FormControl(false),
      language_portugese: new FormControl(false),
      language_italian: new FormControl(false),
      language_indonesian: new FormControl(false)
    })
  }


  save() {
    if (this.filterRulesForm.valid) {
      console.log(this.filterRulesForm.value);
    }
  }

}
