import { Component, OnInit, ViewEncapsulation, TemplateRef, ViewChild  } from '@angular/core';
import { FormGroup, FormControl,Validators, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./ban-list.json');
const data2: any = require('./allowed-list.json');
@Component({
  selector: 'app-word-list',
  templateUrl: './word-list.component.html',
  styleUrls: ['./word-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class wordListComponent implements OnInit {

  banListFormGroup:FormGroup;
  allowedListFormGroup:FormGroup;
  // VALIDATION MESSAGES
  // ============================
  validation_messages = {
    'word': [
      { type: 'required', message: 'Words is required' }
    ]
  };
  update_btn = false;
  records = [];
  records2 = [];

  rows:any = [];
  temp:any = [];
  banList = [];
  rows2:any = [];
  temp2:any = [];
  allowedList = [];
  selectPageValue: number = 10;
  selectPageValue2: number = 10;
  @ViewChild(wordListComponent, {static: true}) table: wordListComponent | undefined;
  @ViewChild(wordListComponent, {static: true}) table2: wordListComponent | undefined;
  constructor(private fb: FormBuilder, private toastr: ToastrService) {
    this.banList = data;
    this.rows = this.banList;
    this.temp = [...this.rows];

    this.allowedList = data2;
    this.rows2 = this.allowedList;
    this.temp2 = [...this.rows2];
  }
  ngOnInit() {
    this.banForm();
    this.allowedForm();
 
  }

  banForm(){
    this.banListFormGroup = this.fb.group({
      id: new FormControl(null),
      word: new FormControl(null, Validators.required),
      csv_file: new FormControl(null),
    })
  }
  allowedForm(){
    this.allowedListFormGroup = this.fb.group({
      id: new FormControl(null),
      word: new FormControl(null, Validators.required),
      csv_file: new FormControl(null),
    })
  }


  uploadListener = (event: any) => {
    // console.log(event);
    const files = event.srcElement.files;
    if (this.isValidCSVFile(files[0])) {
      const input = event.target;
      const reader = new FileReader();
      reader.readAsText(input.files[0]);
      reader.onload = () => {
        const csvData = reader.result;
        // console.log(csvData);
        this.records = (<string>csvData).split(/\r\n|\n/);
        // console.log(this.records);
        this.banListFormGroup.get('word').reset();
      };
      reader.onerror = function () {
        console.log("error is occured while reading file!");
      };
    } else {
      // this.toastr.error('Please import valid .csv file.');
      this.banListFormGroup.get('csv_file').reset();
      this.records = [];
    }
  }

  uploadListener2 = (event: any) => {
    // console.log(event);
    const files = event.srcElement.files;
    if (this.isValidCSVFile(files[0])) {
      const input = event.target;
      const reader = new FileReader();
      reader.readAsText(input.files[0]);
      reader.onload = () => {
        const csvData = reader.result;
        // console.log(csvData);
        this.records2 = (<string>csvData).split(/\r\n|\n/);
        // console.log(this.records);
        this.allowedListFormGroup.get('word').reset();
      };
      reader.onerror = function () {
        console.log("error is occured while reading file!");
      };
    } else {
      // this.toastr.error('Please import valid .csv file.');
      this.allowedListFormGroup.get('csv_file').reset();
      this.records2 = [];
    }
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith('.csv');
  }

  onSubmit(){
    console.log(this.banListFormGroup.value);
  }

  reset() {
    this.banListFormGroup.reset();
  }

  onAllowedFormSubmit(){
    console.log(this.allowedListFormGroup.value);
  }

  allowedFormReset() {
    this.allowedListFormGroup.reset();
  }

  //search filter
  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d:any) {
      return d.banWords.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.banList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table = this.rows;
  }

  editBanList(data:any){

  }

  //search filter
  updateFilter2(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp2.filter(function(d:any) {
      return d.allowedWords.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.allowedList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table2 = this.rows2;
  }

  editAllowedList(data:any){

  }

}
