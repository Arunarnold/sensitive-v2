import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./support.json');
@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SupportComponent implements OnInit {
  // Declaration
  supportForm: FormGroup;
  category_lists = [];
  page = 1;
  pageSize = 5;
  viewData = {};
  closeResult;
  supportUrl = "message";
  supportAllUrl = "message/user";
  supportTicketApi = "message/userticket";
  faqListApi:string = "faq";
  activeId: string = "faq-panel-0";
  faqList = [
    {
      title: "FAQ Title 1",
      description: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident."
    },
    {
      title: "FAQ Title 2",
      description: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident."
    },
    {
      title: "FAQ Title 3",
      description: "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident."
    }
  ];

  // ===============================================================================
  // Validation Part 
  validation_messages = {

    'ticketId': [
      { type: 'required', message: 'Ticket Id is required.' }
    ],
    'category_id': [
      { type: 'required', message: 'Feedback is required.' }
    ],
    'description': [
      { type: 'required', message: 'Description is required.' }
    ],
    'subject': [
      { type: 'required', message: 'Subject is required.' }
    ]
  };
  
  rows:any = [];
  temp = [];
  feedbackList = [];
  selectPageValue: number = 10;
  rows2 = [];
  @ViewChild(SupportComponent, {static: true}) table: SupportComponent;
  constructor(public fb: FormBuilder, public service: GlobalServiceService, public toastr: ToastrService) {
    this.feedbackList = data;
    this.rows = this.feedbackList;
    this.temp = [...this.rows];
  }

  ngOnInit(): void {
    this.supportFormInit();
    this.getList();
    this.getFaqList();
    this.category_lists = [{
      id: 1,
      categoryList: "general"
    },
    {
      id: 2,
      categoryList: "billing"
    },
    {
      id: 3,
      categoryList: "technical"
    }];
  }
  toggleAccordian(event) {
    console.log(event.panelId);
    // If it is already open you will close it and if it is closed open it
    this.activeId = this.activeId == event.panelId ? "" : event.panelId;
}
  // ==============================================================

  // FormInit
  supportFormInit = () => {
    this.supportForm = this.fb.group({
      id: new FormControl(null),
      ticketId: new FormControl({value: null, disabled: true}),
      subject: new FormControl(null, Validators.required),
      category_id: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required)
    });
  }

  // ============================================================================

  // Crud
  send() {
    console.log(this.supportForm.value)
    if (this.supportForm.valid) {
      let obj:any = {
        subject : this.supportForm.value.subject,
        message : this.supportForm.value.description,
        category: this.supportForm.value.category_id
      }
      if(this.supportForm.value != null){
        obj.ticketId =  this.supportForm.get('ticketId').value
      }
    console.log(obj)

      this.service.post_data(this.supportUrl, obj).subscribe((result) => {
        console.log(result);
        this.toastr.success("Data Sended Successfully");
        this.getList();
        this.resetForm();
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      })
    }
    else {
      this.service.validateAllFormControl(this.supportForm)
    }
  }

  getList() {
    this.service.get_lists(this.supportAllUrl).subscribe((result) => {
      console.log(result)
      if(result != null || result != undefined){
        this.feedbackList = [];
        result.forEach(element => {
          let totalArrayData = {
            categoryName: element.category,
            subject: element.subject,
            lastUpdated: this.service.formattedDate(element.updatedAt),
            description: element.message,
            status: element.status,
            ticketId: element.ticketId,
            id: element._id
          }
          this.feedbackList.push(totalArrayData);
        });
        console.log(this.feedbackList);
        this.rows = this.feedbackList;
        this.temp = [...this.feedbackList];
      }
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
  }

  resetForm() {
    this.supportForm.reset();
  }

  viewSupport(modalContent, row){
    if(modalContent){
      this.service.get_data(this.supportTicketApi, row.ticketId).subscribe((result) => {
        if(result != null || result != undefined){
        console.log(result)
        let feedbackModalList = [];
        result.forEach(element => {
          let totalArrayData = {
            categoryName: element.category,
            subject: element.subject,
            lastUpdated: this.service.formattedDate(element.updatedAt),
            description: element.message,
            status: element.status,
            ticketId: element.ticketId,
            id: element._id
          }
          feedbackModalList.push(totalArrayData);
        });
        console.log(feedbackModalList);
        this.rows2 = feedbackModalList;
        // this.modalService.open(modalContent, { ariaLabelledBy: 'modal-basic-title', size: 'lg' });
        }
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      })
    }
  }

  editSupport(row){
    if(row){
      this.service.get_data(this.supportTicketApi, row.ticketId).subscribe((result) => {
        console.log(result);
        result.forEach(element => {
          if(element.isOrigin == true){
            this.supportForm.patchValue({
              id: element._id,
              ticketId: element.ticketId,
              subject: element.subject,
              category_id: element.category,
              description: element.message
            })
          }
      })
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
    }
  }

  // =======================================================================================================
  // ====================================================================
    //search filter
    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return d.feedback.toLowerCase().indexOf(val) !== -1 || !val;
      });
  
      // update the rows
      this.feedbackList = temp;
      // Whenever the filter changes, always go back to the first page
      this.table = this.rows;
    }

    getFaqList(){
      this.service.get_lists(this.faqListApi).subscribe((result) => {
        console.log(result)
        if(result != null || result != undefined){
          this.faqList = [];
          result.forEach(element => {
            let totalArrayData = {
              title: element.question,
              description: element.answer
            }
            this.faqList.push(totalArrayData);
          });
        }
      })
    }
}
