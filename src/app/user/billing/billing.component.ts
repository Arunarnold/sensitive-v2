import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./billing.json');
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit {
  bsValue = new Date();
  maxDate = new Date();


  rows:any = [];
  temp:any = [];
  billingList = [];
  selectPageValue: number = 10;
  @ViewChild(BillingComponent, {static: true}) table: BillingComponent | undefined;
  constructor(private service: GlobalServiceService, private toastr: ToastrService) {
    this.billingList = data;
    this.rows = this.billingList;
    this.temp = [...this.rows];

    // date picker
    this.maxDate.setDate(this.maxDate.getDate() + 7);

  }
  ngOnInit() {

  }
  onBillingSubmit(){

  }
  //search filter
  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d:any) {
      return d.planName.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.billingList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table = this.rows;
  }

  editBillingList(){

  }


}
