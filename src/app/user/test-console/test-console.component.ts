import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { GlobalServiceService } from '../../shared/global-service.service';
@Component({
  selector: 'app-test-console',
  templateUrl: './test-console.component.html',
  styleUrls: ['./test-console.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TestConsoleComponent implements OnInit {

  constructor(public fb: FormBuilder, private service: GlobalServiceService) {
  }

  ngOnInit() {
    this.testConsoleFormInit();
  }
  // Declaration
  // =======================
  testConsoleForm: FormGroup;
  testConsoleUrl = "test_console";
  // =================================================================
  // FormInit
  // =======================
  testConsoleFormInit = () => {
    this.testConsoleForm = this.fb.group({
      id: new FormControl(null),
      method_id: new FormControl(1, Validators.required),
      text: new FormControl(null, Validators.required),
      replace_symbol: new FormControl(null, Validators.required),
      word: new FormControl(null, Validators.required),
      deep_search: new FormControl(false),
      filter_urls: new FormControl(false),
      filter_emails: new FormControl(false),
      filter_phone_numbers: new FormControl(false),
      response_text_id: new FormControl(1, Validators.required),
      request_url: new FormControl(null),
      service_response: new FormControl(null),
    });
  }
  // =================================================================
  // VALIDATION
  // ================================================================
  validation_messages = {
    'method_id': [
      { type: 'required', message: 'Method is required.' }
    ],
    'text': [
      { type: 'required', message: 'Text is required.' }
    ],
    'replace_symbol': [
      { type: 'required', message: 'Replace Symbol is required.' }
    ],
    'word': [
      { type: 'required', message: 'Word is required.' },
    ],
    'response_text_id': [
      { type: 'required', message: 'Response Text is required.' },
    ],
    'request_url': [
      { type: 'required', message: 'Request url is required.' },
    ],
    'service_response': [
      { type: 'required', message: 'Service response is required.' },
    ]
  };

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  // =======================================================================================================
  // Crud
  // =============================
  reset() {
    this.testConsoleForm.reset();
  }
  save() {
    if (this.testConsoleForm.valid) {
      this.service.post_data(this.testConsoleUrl, this.testConsoleForm.value).subscribe((result) => {
        this.testConsoleForm.get('request_url').patchValue("acestranetworks.com");
        this.testConsoleForm.get('service_response').patchValue("Success");
      })
    }
    else {
      this.validateAllFormFields(this.testConsoleForm);
    }
  }
  // =================================================================
}
