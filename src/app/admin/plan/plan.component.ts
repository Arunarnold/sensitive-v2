import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./plan.json');
@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlanComponent implements OnInit {

  // Declaration
  planform: FormGroup;
  planApi:string = "plan";
  planListApi:string = "plan/all";

  // page = 1;
  // pageSize = 5;
  // viewPlan = {};
  closeResult;
  // update_btn = false;

  rows:any = [];
  temp = [];
  planList = [];
  selectPageValue: number = 10;
  // ================================================================
  // VALIDATION MESSAGES
  validation_messages = {
    'plan_name': [
      { type: 'required', message: 'Plan Name is required.' }
    ],
    'isFeatured': [
      { type: 'required', message: 'Featured is required.' }
    ],
    'status_id': [
      { type: 'required', message: 'Plan Status is required.' }
    ],
    'description': [
      { type: 'required', message: 'Point is required.' }
    ],
    'plan_order': [
      { type: 'required', message: 'Point is required.' }
    ],
  };
  @ViewChild(PlanComponent, {static: true}) table: PlanComponent;
  constructor(public fb: FormBuilder, public service: GlobalServiceService, public toastr: ToastrService) {
    this.planList = data;
    this.rows = this.planList;
    this.temp = [...this.rows];
  }

  ngOnInit(): void {
    this.PlansFormInit();
    this.getPlanList();
  }

  // =========================================================================================
  PlansFormInit = () => {
    this.planform = this.fb.group({
      id: new FormControl(null),
      plan_name: new FormControl(null, Validators.required),
      status_id: new FormControl(false, Validators.required),
      description: new FormControl(null, Validators.required),
      plan_order: new FormControl(null, Validators.required),
      // isFeatured: new FormControl(false, Validators.required),
      // description: this.fb.array([
      //   this.description()
      // ]),
    });
  }
  // description() {
  //   return this.fb.group({
  //     point: new FormControl(null, Validators.required)
  //   })
  // }
  // addPoints() {
  //   const control = this.planform.get('description') as FormArray;
  //   if (control.length < 5) {
  //     control.push(this.description());
  //   }
  // }
  // deletePoints(i) {
  //   const control = this.planform.get('description') as FormArray;
  //   if (control.length > 1) {
  //     control.removeAt(i);
  //   }
  // }

  getPlanList() {
    this.service.get_lists(this.planListApi).subscribe((result) => {
      console.log(result);
      if(result != null || result != undefined){
        this.planList = [];
        result.forEach(element => {
          let totalArrayData = {
            planName: element.planName,
            planOrder: element.planOrder,
            status: element.status,
            id: element._id
          }
          this.planList.push(totalArrayData);
        });
        console.log(this.planList);
        this.rows = this.planList;
        this.temp = [...this.planList];
      }
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
  }


  onSubmitPlanForm(){
    if (this.planform.valid) {
      let data = {
        planName: this.planform.value.plan_name,
        planOrder: this.planform.value.plan_order,
        planDescription : this.planform.value.description,
        status: (this.planform.value.status_id == false) ? 'inactive' : 'active',
        // isFeatured : this.planform.value.isFeatured
      }
      if (this.planform.get('id').value) {
        this.service.put_data(this.planApi, this.planform.get('id').value, data).subscribe((result) => {
          if(result){
          this.getPlanList();
          this.refreshBtn();
          this.toastr.success("Plan Updated Successfully", this.planform.value.plan_name);
          }
        },(error)=>{
          if(error.status == 422){
            this.toastr.error(error.error.errors.msg);  
          }else if(error.status == 0){
            this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
          }
          console.log(error);
        })
      } else {
        this.service.post_data(this.planApi, data).subscribe((result) => {
          if(result){
          this.getPlanList();
          this.refreshBtn();
          this.toastr.success("Plan Added Successfully", this.planform.value.plan_name);
          }
        },(error)=>{
          if(error.status == 422){
            this.toastr.error(error.error.errors.msg);  
          }else if(error.status == 0){
            this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
          }
          console.log(error);
        })
      }
    }else{
      this.service.validateAllFormControl(this.planform);
    }
  }


  
  // getSingleData(id, content?: any) {
  //   this.service.get_data(this.planApi, id).subscribe((result) => {
  //     if (content) {
  //       this.viewPlan = result;
  //       // console.log(this.viewPlan)
  //       // this.value(content);
  //     }
  //     else {
  //       this.clearFormArray();
  //       const control = this.planform.get('description') as FormArray;
  //       this.planform.patchValue({
  //         id: result._id,
  //         plan_name: result.planName,
  //         plan_price: result.planPrice,
  //         status_id: result.status,
  //         description: result.description ? result.description : ''
  //       })
  //       result.description.forEach((element, i) => {
  //         control.push(this.fb.group({
  //           point: element.point
  //         }))
  //       });
  //       this.update_btn = true;
  //     }
  //   })
  // }

  editPlan(data){
    if(data){
      this.service.get_data(this.planApi, data.id).subscribe((result) => {
        console.log(result);
        this.planform.patchValue({
          id: result._id,
          plan_name: result.planName,
          plan_order: result.planOrder,
          status_id: (result.status == 'active')? true : false,
          description: result.planDescription ? result.planDescription : '',
          // isFeatured : result.isFeatured
        })
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
    }
  }

  refreshBtn(){
    this.planform.reset({
      status_id: false,
      isFeatured: false,
    });
  }

  //adding plan featured active class
  showActiveColumn({ row, column, value }){
    return (row.isFeatured == true) ? 'isFeaturedActive' : '';
  }
  // resetForm() {
  //   const control = this.planform.get('description') as FormArray;
  //   this.clearFormArray();
  //   this.planform.reset();
  //   control.push(this.description());
  // }
  // clearFormArray() {
  //   const control = this.planform.get('description') as FormArray;
  //   while (control.length != 0) {
  //     control.removeAt(0);
  //   }
  // }


  viewPlan(modalElement, data){
    // if(modalElement){
    //   let data = {
    //     domain: domainname
    //   }
    //   this.service.postConfig(this.getDateListApi, data)
    //   .subscribe((data: any) => {
    //     if(data != null || data != undefined){
    //       let dateArray = [];
    //       data.forEach(value => {
    //         dateArray.push({domainDate: this.service.formattedDate(value)});
    //       });
    //       this.modalService.open(modalElement, { size: 'lg' });
    //       this.modalSubTitle = domainname;
    //       this.rows2 = dateArray;
    //     }else{
    //       console.log("Not Valid");
    //     }
    //   },(error)=>{
    //     if(error.status == 0){
    //       this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
    //     }
    //     console.log(error);
    //   });
    // }
  }


    //search filter
    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return d.planName.toLowerCase().indexOf(val) !== -1 || !val;
      });
  
      // update the rows
      this.planList = temp;
      // Whenever the filter changes, always go back to the first page
      this.table = this.rows;
    }


    // deletePlan(row){
    //   // you need this due to  change-detection
    //   this.planList = this.arrayRemove(this.planList, row.id);
    //   this.service.delete_data(this.planApi, row.id)
    //   .subscribe((data: any) => {
    //     if(data != null || data != undefined){
    //       this.toastr.success("Plan is Deleted Successfully");
    //     }
    //   },(error)=>{
    //     if(error.status == 0){
    //       this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
    //     }
    //     console.log(error);
    //   });
    // }

    // arrayRemove(array, rowName) {
    //   return array.filter(function(element){
    //       return element.id != rowName;
    //   });
    // }
}
