import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./subscriptions.json');
const todayDate = new Date();

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.scss'],
})

export class SubscriptionsComponent implements OnInit {
  rows:any = [];
  temp = [];
  subscriptionForm: FormGroup;
  fromDateModel;
  toDateModel;
  subscriptionsList = [];
  selectPageValue: number = 10;
  subscriptionGraphApi:string = "admin/subscriptions";
  showView: boolean = false;
  multi_line = [
    {
      name: 'Gold',
      series: [
        {
          name: '2018',
          value: 7300000,
        },
        {
          name: '2019',
          value: 8940000,
        },
        {
          name: '2020',
          value: 6490000,
        },
      ],
    },

    {
      name: 'Silver',
      series: [
        {
          name: '2018',
          value: 7870000,
        },
        {
          name: '2019',
          value: 8270000,
        },
        {
          name: '2020',
          value: 6090000,
        },
      ],
    },

    {
      name: 'Basic',
      series: [
        {
          name: '2018',
          value: 5000002,
        },
        {
          name: '2019',
          value: 5800000,
        },
        {
          name: '2020',
          value: 7490000,
        },
      ],
    },
  ];
  single = [
    {
      name: 'Gold',
      value: 35,
    },
    {
      name: 'Silver',
      value: 30,
    },
    {
      name: 'Basic',
      value: 35,
    },
  ];
  view: any[] = [700, 400];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'Count';

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  //  showYAxisLabel: boolean = true;
  //  showXAxisLabel: boolean = true;
  //  xAxisLabel: string = 'Year';
  //  yAxisLabel: string = 'Population';
  timeline: boolean = true;

  colorScheme_line = {
    domain: ['#ffd898', '#6e768d', '#f15a22' , '#7aa3e5', '#a8385d', '#aae3f5'],
  };
  // options
  // gradient: boolean = true;
  // showLegend: boolean = true;
  // showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';
  colorScheme_pie = {
    domain: ['#ffd898', '#6e768d', '#f15a22' , '#AAAAAA'],
    
  };
  @ViewChild(SubscriptionsComponent, {static: true}) table: SubscriptionsComponent;
  constructor( public fb: FormBuilder, public service: GlobalServiceService, public toastr: ToastrService) {
    // this.fromDateModel = { year: todayDate.getFullYear(), month: todayDate.getMonth() + 1, day: 1 };
    // this.toDateModel = { year: todayDate.getFullYear(), month: todayDate.getMonth() + 1, day: todayDate.getDate() };

    this.subscriptionsList = data;
    this.rows = this.subscriptionsList;
    this.temp = [...this.rows];
  }

  ngOnInit(): void {
    this.subscriptionFormInit();
  }

  subscriptionFormInit(){
    this.subscriptionForm = this.fb.group({
      id: new FormControl(1),
      from_date: new FormControl(this.fromDateModel, Validators.required),
      to_date: new FormControl(this.toDateModel, Validators.required),
    });
  }


  onSubmitSubscriptionForm(){
    console.log(this.subscriptionForm.value);
      if(this.subscriptionForm.valid){
        console.log(this.subscriptionForm.value);
        let data = {
          fromDate : this.subscriptionForm.value.from_date
        }
        this.service.post_data(this.subscriptionGraphApi, data).subscribe((result)=>{
          console.log(result);
          if(result != null || result != undefined){
            // pie chart graph
            if(result.gold || result.silver || result.basic){
              this.single = [
                {
                  name: 'Gold',
                  value: result.gold || 0,
                },
                {
                  name: 'Silver',
                  value: result.silver || 0,
                },
                {
                  name: 'Basic',
                  value: result.basic || 0,
                },
              ];
            }else{
              this.single = [];
            }
  
            //line chart
            let subscriptionLineGraph = [];
            let totalSubscriptionLineGraph = [];
            this.multi_line = [];
            result.subscriptions.forEach(element => {
              let totalArrayData = {
                name: element.date,
                value: element.count,
              }
              subscriptionLineGraph.push(totalArrayData);
            });
            console.log(subscriptionLineGraph);
  
            result.subscriptions.forEach(element => {
              let totalArrayData = {
                name: this.service.formattedYear(element.date),
                series: subscriptionLineGraph,
              }
              totalSubscriptionLineGraph.push(totalArrayData);
            })
  
            this.multi_line = totalSubscriptionLineGraph;
  
  
            // table display
  
            this.subscriptionsList = [];
            result.data.forEach(element => {
              let totalArrayData = {
                email: element.userEmail,
                type: element.planPeriod,
                package: element.planPackage,
                amount: element.planAmount,
                registeredDate: this.service.formattedDate(element.createdAt),
                subscriptionId: element.subscriptionId,
                planCurrency: element.planCurrency,
                paid: element.isPaid,
                status: element.status,
                id: element._id
    
              }
              this.subscriptionsList.push(totalArrayData);
            });
            console.log(this.subscriptionsList);
            this.rows = this.subscriptionsList;
            this.temp = [...this.subscriptionsList];


            this.showView = true;
          }
        },(error)=>{
          if(error.status == 0){
            this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
          }
          console.log(error);
        })
      }
  }

    //search filter
    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return d.email.toLowerCase().indexOf(val) !== -1 || !val;
      });
  
      // update the rows
      this.subscriptionsList = temp;
      // Whenever the filter changes, always go back to the first page
      this.table = this.rows;
    }

    //view file
    viewFile(row){
      console.log(row);
    }


}
