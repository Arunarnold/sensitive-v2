import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./inbox.json');

const todayDate = new Date();
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  // Declaration
  inboxUrl = "message";
  inboxAllUrl = "message/allusers";
  inboxSearchApi = "message/search";
  closeResult;
  selectPageValue: number = 15;
  rows:any = [];
  temp = [];
  inboxList = [];
  fromDateModel;
  toDateModel;
  inboxForm: FormGroup;
  inboxFilterForm: FormGroup;
  category_lists = [];
  update_btn = false;
  // ============================================================================================
  // VALIDATION MESSAGES
  // ==========================================
  validation_messages = {
    'ticketId': [
      { type: 'required', message: 'Ticket ID is required.' },
    ],
    'subject': [
      { type: 'required', message: 'Subject is required.' },
    ],
    'category_id': [
      { type: 'required', message: 'Category is required.' },
    ],
    'answer': [
      { type: 'required', message: 'answer is required.' },
    ],
    'status': [
      { type: 'required', message: 'status is required.' },
    ],
    'id': [
      { type: 'required', message: 'Ticket ID is required.' },
    ],
  };

  @ViewChild(InboxComponent, {static: true}) table: InboxComponent;
  constructor(public fb: FormBuilder, public service: GlobalServiceService, public toastr: ToastrService) {
    let first = todayDate.getDate() - todayDate.getDay(); // First day is the day of the month - the day of the week
    let last = first + 6; // last day is the first day + 6
    let startDate = new Date(todayDate.setDate(first));
    var endDate = new Date(todayDate.setDate(last));
    this.fromDateModel = { year: startDate.getFullYear(), month: startDate.getMonth() + 1, day: startDate.getDate()};
    this.toDateModel = { year: endDate.getFullYear(), month: endDate.getMonth() + 1, day: endDate.getDate() };
    this.inboxList = data;
    this.rows = this.inboxList;
    this.temp = [...this.rows]; 
  }

  ngOnInit(): void {
    this.getList();
    this.formInit();
    this.category_lists = [{
      id: 1,
      categoryList: "General",
      categoryValue: "general"
    },
    {
      id: 2,
      categoryList: "Billing",
      categoryValue: "billing"
    },
    {
      id: 3,
      categoryList: "Technical",
      categoryValue: "technical"
    }];
  }

  // FormInit
 formInit = () => {
    this.inboxForm = this.fb.group({
      id: new FormControl(null),
      ticketId: new FormControl({value: null, disabled: true}, Validators.required),
      subject: new FormControl(null, Validators.required),
      category_id: new FormControl(null, Validators.required),
      answer: new FormControl(null, Validators.required),
      status: new FormControl(false, Validators.required)
    });

    this.inboxFilterForm = this.fb.group({
      id: new FormControl(null, Validators.required),
      status: new FormControl(null, Validators.required)
    })
  }

  // Crud
  getList() {
    this.service.get_lists(this.inboxAllUrl).subscribe((result) => {
      console.log(result)
      if(result != null || result != undefined){
        this.inboxList = [];
        result.forEach(element => {
          let totalArrayData = {
            category: element.category,
            subject: element.subject,
            lastUpdated: this.service.formattedDate(element.updatedAt),
            comments: element.message,
            email: element.email,
            status: element.status,
            ticketNo: element.ticketId,
            id: element._id
          }
          this.inboxList.push(totalArrayData);
        });
        console.log(this.inboxList);
        this.rows = this.inboxList;
        this.temp = [...this.inboxList];
      }
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
    })

  }

  reply(data){
    if(data){
      this.service.get_data(this.inboxUrl, data.id).subscribe((result) => {
        console.log(result);
        this.inboxForm.patchValue({
          id: result._id,
          ticketId: result.ticketId,
          answer: result.message,
          subject: result.subject,
          status: (result.status == 'solved')? true : false,
          category_id: result.category
        })
        this.update_btn = true;
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
    }
  }

  onSubmitInboxForm(){
    if(this.inboxForm.valid){
      console.log(this.inboxForm.value);
      let obj = {
        subject : this.inboxForm.value.subject,
        message : this.inboxForm.value.answer,
        category: this.inboxForm.value.category_id,
        status: (this.inboxForm.value.status == 'true')? 'solved' : 'open',
        ticketId: this.inboxForm.get('ticketId').value
      }
      if (this.inboxForm.get('id').value) {
        this.service.put_data(this.inboxUrl, this.inboxForm.get('id').value, obj).subscribe((result) => {
          if(result){
            this.toastr.success("Data Sended Successfully");
            this.getList();
            this.resetForm();
            this.update_btn = false;
          }
        },(error)=>{
          if(error.status == 422){
            this.toastr.error(error.error.errors.msg);  
          }else if(error.status == 0){
            this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
          }
          console.log(error);
        })
      } else {
        this.service.post_data(this.inboxUrl, obj).subscribe((result) => {
          console.log(result);
          this.toastr.success("Data Sended Successfully");
          this.getList();
          this.resetForm();
        },(error)=>{
          if(error.status == 0){
            this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
          }
          console.log(error);
        })
      }
    }else{
      this.service.validateAllFormControl(this.inboxForm);
    }
  }

  onSubmitInboxFilterForm(){
    if(this.inboxFilterForm.valid){
      console.log(this.inboxFilterForm.value);
      let obj = {
        ticketId: this.inboxFilterForm.value.id,
        status: this.inboxFilterForm.value.status
      }
      this.service.post_data(this.inboxSearchApi, obj).subscribe((result) => {
        console.log(result);
        this.inboxList = [];
        result.forEach(element => {
          let totalArrayData = {
            category: element.category,
            subject: element.subject,
            lastUpdated: this.service.formattedDate(element.updatedAt),
            comments: element.message,
            email: element.email,
            status: element.status,
            ticketNo: element.ticketId,
            id: element._id
          }
          this.inboxList.push(totalArrayData);
        });
        console.log(this.inboxList);
        this.rows = this.inboxList;
        this.temp = [...this.inboxList];
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      })
    }else{
      this.service.validateAllFormControl(this.inboxFilterForm);
    }
  }

  resetFilter() {
    this.inboxFilterForm.reset();
  }

  resetForm() {
    this.inboxForm.reset();
  }
  // ===================================================================================
}
