import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./plan-price.json');
@Component({
  selector: 'app-plan-price',
  templateUrl: './plan-price.component.html',
  styleUrls: ['./plan-price.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlanPriceComponent implements OnInit {
  iconColor:string = "#7b93a4";
  // Declaration
  planPriceForm: FormGroup;
  planPriceApi = "price";
  planPriceListApi = "price/all";
  getPlanAllApi = "plan/all";
  currencyAllApi = "currency/all";
  planNameList:any;
  intervalList:any;
  currencyList:any;
  planList = [];
  // ================================================================
  // VALIDATION MESSAGES
  validation_messages = {
    'plan_name': [
      { type: 'required', message: 'Plan Name is required.' }
    ],
    'plan_price': [
      { type: 'required', message: 'Plan Price is required.' }
    ],
    'price_interval': [
      { type: 'required', message: 'Price Interval is required.' }
    ],
    'currency': [
      { type: 'required', message: 'Currency is required.' }
    ],
    'price_status': [
      { type: 'required', message: 'Price Status is required.' }
    ],
    'isFeatured': [
      { type: 'required', message: 'Feature is required.' }
    ]
  };
  
  rows:any = [];
  temp = [];
  planPriceList = [];
  selectPageValue: number = 10;
  @ViewChild(PlanPriceComponent, {static: true}) table: PlanPriceComponent;
  constructor(public fb: FormBuilder, public service: GlobalServiceService, public toastr: ToastrService) {
    this.planPriceList = data;
    this.rows = this.planPriceList;
    this.temp = [...this.rows];
  }

  ngOnInit(): void {
    this.planNameList = [
      {
        stripeId: 1,
        planName: "basic"
      },
      {
        stripeId: 2,
        planName: "silver"
      },
      {
        stripeId: 3,
        planName: "gold"
      }
    ];
    this.intervalList = [
      {
      id: 1,
      interval: "Monthly",
      intervalValue: "month"
      },
      {
      id: 2,
      interval: "Yearly",
      intervalValue: "year"
      }
    ];
    this.currencyList = [
      {
        id: 1,
        currency_name: "Canadian Dollar",
        currency_code: "CAD"
      }, {
        id: 2,
        currency_name: "Ameriacan Dolar",
        currency_code: "USD"
      }, {
        id: 3,
        currency_name: "Euro",
        currency_code: "EURO"
      },
      {
        id: 4,
        currency_name: "Indian",
        currency_code: "INR"
      }
    ];

    this.getCurrencyList();
    this.planPriceFormInit();
    this.getplanPriceList();
    this.getPlanList();
  }

  // =========================================================================================
  planPriceFormInit(){
    this.planPriceForm = this.fb.group({
      id: new FormControl(null),
      plan_name: new FormControl(null, Validators.required),
      plan_price: new FormControl(null, Validators.required),
      price_interval: new FormControl(null, Validators.required),
      currency: new FormControl(null, Validators.required),
      stripeId: new FormControl(null, Validators.required),
      isFeatured: new FormControl(false, Validators.required),
      price_status: new FormControl(true)
    });
  }
  
  getCurrencyList(){
    this.service.get_lists(this.currencyAllApi).subscribe((result) => {
      console.log(result);
      if(result != null || result != undefined){
        this.currencyList = [];
        result.forEach(element => {
          let totalArrayData = {
            currency_name: element.name,
            currency_code: element.code,
            id: element._id,
          }
          this.currencyList.push(totalArrayData);
        });
        console.log(this.currencyList);
      }
      });
  }

  getPlanList() {
    this.service.get_lists(this.getPlanAllApi).subscribe((result) => {
      console.log(result);
      if(result != null || result != undefined){
        this.planNameList = [];
        result.forEach(element => {
          let totalArrayData = {
            planName: element.planName,
            stripeId: element.planStripeId
          }
          this.planNameList.push(totalArrayData);
        });
        console.log(this.planNameList);
        this.rows = this.planNameList;
        this.temp = [...this.planList];
      }
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
  }

  // selecting stripe id
  onOptionsSelected(event){
    let stripeId = event.target[event.target.selectedIndex].dataset.stripeid;
    this.planPriceForm.patchValue({
      stripeId : stripeId
    });
  }

  getplanPriceList() {
    this.service.get_lists(this.planPriceListApi).subscribe((result) => {
      console.log(result);
      if(result != null || result != undefined){
        this.planPriceList = [];
        result.forEach(element => {
          let totalArrayData = {
            planName: element.planName,
            price: element.planPrice,
            currency: element.currency,
            interval: element.priceInterval,
            status: element.status,
            stripeId: element.priceStripeId,
            isFeatured: element.isFeatured,
            id: element._id
          }
          this.planPriceList.push(totalArrayData);
        });
        console.log(this.planPriceList);
        this.rows = this.planPriceList;
        this.temp = [...this.planPriceList];
      }
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
  }

  // color change dectection
  changeGradientColor(colorData){
    this.planPriceForm.patchValue({
      color: colorData
    })
  }

  onSubmitPlanPriceForm() {
   console.log(this.planPriceForm.value);
    if (this.planPriceForm.valid) {
      let postData = {
        planName: this.planPriceForm.value.plan_name,
        planPrice: this.planPriceForm.value.plan_price,
        currency : this.planPriceForm.value.currency,
        priceInterval: this.planPriceForm.value.price_interval,
        planStripeId: this.planPriceForm.value.stripeId,
        isFeatured: this.planPriceForm.value.isFeatured,
        status: (this.planPriceForm.value.price_status == false) ? 'inactive' : 'active'
      }

        this.service.post_data(this.planPriceApi, postData).subscribe((result) => {
          if(result){
            this.toastr.success("Category Added Successfully", this.planPriceForm.value.category_name);
            this.getplanPriceList();
            this.resetForm();
          }
        },(error)=>{
          if(error.status == 422){
            this.toastr.error(error.error.errors.msg);  
          }else if(error.status == 0){
            this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
          }
          console.log(error);
        })
      
    }
    else {
      this.service.validateAllFormControl(this.planPriceForm);
    }
  }

  // editplanPrice(data){
  //   if(data){
  //     this.service.get_data(this.planPriceApi, data.id).subscribe((result) => {
  //       console.log(result);
  //       this.planPriceForm.patchValue({
  //         id: result._id,
  //         plan_name: result.planName,
  //         plan_price: result.iconColor,
  //         price_interval: result.expense,
  //         currency: result.expense,
  //         price_status: (result.status == 'active')? true : false,
  //         stripeId: result.planPriceName
  //       })
  //       this.update_btn = true;
  //     },(error)=>{
  //       if(error.status == 0){
  //         this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
  //       }
  //       console.log(error);
  //     });
  //   }
  // }

  resetForm() {
    this.planPriceForm.reset({
      isFeatured: false,
      price_status: true
    });
  }

  planActivate(event, row){
    console.log(event);
    let postData = {
      priceStripeId: row.stripeId,
      status: (event == true)? 'active' : 'inactive'
    }
    console.log(postData);

      this.service.put_data(this.planPriceApi, row.id, postData).subscribe((result) => {
        if(result){
          this.toastr.success("Category Updated Successfully", this.planPriceForm.value.category_name);
          this.getplanPriceList();
        }
      },(error)=>{
        if(error.status == 422){
          this.toastr.error(error.error.errors.msg);  
        }else if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      })
  }

  //adding plan featured active class
  showActiveColumn({ row, column, value }){
    return (row.isFeatured == true) ? 'isFeaturedActive' : '';
  }

    //search filter
    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return d.planName.toLowerCase().indexOf(val) !== -1 || !val;
      });
  
      // update the rows
      this.planPriceList = temp;
      // Whenever the filter changes, always go back to the first page
      this.table = this.rows;
    }

    deleteplanPrice(row){
      // you need this due to  change-detection
      this.planPriceList = this.arrayRemove(this.planPriceList, row.id);
      this.service.delete_data(this.planPriceApi, row.id)
      .subscribe((data: any) => {
        if(data != null || data != undefined){
          this.toastr.success("Category is Deleted Successfully");
        }
      },(error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      });
    }

    arrayRemove(array, rowName) {
      return array.filter(function(element){
          return element.id != rowName;
      });
    }
}
