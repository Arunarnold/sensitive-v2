import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
declare var require: any;
const data: any = require('./billing.json');
const todayDate = new Date();
@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss'],
})
export class BillingComponent implements OnInit {
  billingGraphApi = 'admin/billings';
  viewPlan = {};
  page = 1;
  pageSize = 5;
  closeResult;
  rows:any = [];
  temp = [];
  billingList = [];
  selectPageValue: number = 10;
  billingForm: FormGroup;
  fromDateModel;
  toDateModel;
  showView: boolean = false;
  multi_line = [
    {
      name: 'Gold',
      series: [
        {
          name: '2018',
          value: 7300000,
        },
        {
          name: '2019',
          value: 8940000,
        },
        {
          name: '2020',
          value: 6490000,
        },
      ],
    },

    {
      name: 'Silver',
      series: [
        {
          name: '2018',
          value: 7870000,
        },
        {
          name: '2019',
          value: 8270000,
        },
        {
          name: '2020',
          value: 6090000,
        },
      ],
    },

    {
      name: 'Basic',
      series: [
        {
          name: '2018',
          value: 5000002,
        },
        {
          name: '2019',
          value: 5800000,
        },
        {
          name: '2020',
          value: 7490000,
        },
      ],
    },
  ];
  single = [
    {
      name: 'Gold',
      value: 35,
    },
    {
      name: 'Silver',
      value: 30,
    },
    {
      name: 'Basic',
      value: 35,
    },
  ];
  view: any[] = [700, 400];

  // options
  showXAxis: boolean = true;
  showYAxis: boolean = true;
  gradient: boolean = true;
  showLegend: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  showYAxisLabel: boolean = true;
  yAxisLabel: string = 'Count';

  colorScheme = {
    domain: ['#35a989', '#f5b225', '#2a3142'],
  };
  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  //  showYAxisLabel: boolean = true;
  //  showXAxisLabel: boolean = true;
  //  xAxisLabel: string = 'Year';
  //  yAxisLabel: string = 'Population';

  colorScheme_line = {
    domain: ['#ffd898', '#6e768d', '#f15a22' , '#7aa3e5', '#a8385d', '#aae3f5'],
  };

  // options
  // gradient: boolean = true;
  // showLegend: boolean = true;
  // showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'below';
  legendTitle_pie: string = 'a';
  colorScheme_pie = {
    domain: ['#ffd898', '#6e768d', '#f15a22' , '#AAAAAA'],
  };

  @ViewChild(BillingComponent, {static: true}) table: BillingComponent;
  constructor(
    public fb: FormBuilder,
    public service: GlobalServiceService,
    public toastr: ToastrService
  ) {
    // this.fromDateModel = { year: todayDate.getFullYear(), month: todayDate.getMonth() + 1, day: 1 };
    // this.toDateModel = { year: todayDate.getFullYear(), month: todayDate.getMonth() + 1, day: todayDate.getDate() };
    this.billingList = data;
    this.rows = this.billingList;
    this.temp = [...this.rows];
  }

  ngOnInit(): void {
    this.billingFormInit();
  }

  billingFormInit(){
    this.billingForm = this.fb.group({
      id: new FormControl(1),
      from_date: new FormControl(this.fromDateModel, Validators.required),
      to_date: new FormControl(this.toDateModel, Validators.required),
    });
  }

  onSubmitBillingForm(){
    console.log(this.billingForm.value);
      // if(this.billingForm.valid){
      //   console.log(this.billingForm.value);
      //   let data = {
      //     fromDate : this.parserFormatter.format(this.billingForm.value.from_date)
      //   }
      //   this.service.post_data(this.billingGraphApi, data).subscribe((result)=>{
      //     console.log(result);
      //     if(result != null || result != undefined){
      //       // pie chart graph
      //       if(result.gold || result.silver || result.basic){
      //         this.single = [
      //           {
      //             name: 'Gold',
      //             value: result.gold || 0,
      //           },
      //           {
      //             name: 'Silver',
      //             value: result.silver || 0,
      //           },
      //           {
      //             name: 'Basic',
      //             value: result.basic || 0,
      //           },
      //         ];
      //       }else{
      //         this.single = [];
      //       }
    
      //       //line chart graph
      //       let billingLineGraph = [];
      //       let totalBillingLineGraph = [];
      //       this.multi_line  = [];
      //       result.billings.forEach(element => {
      //         let totalArrayData = {
      //           name: element.date,
      //           value: element.count,
      //         }
      //         billingLineGraph.push(totalArrayData)
      //       });
      //       console.log(billingLineGraph);
    
      //       result.billings.forEach(element => {
      //         let totalArrayData = {
      //           name: this.service.formattedYear(element.date),
      //           series: billingLineGraph,
      //         }
      //         totalBillingLineGraph.push(totalArrayData);
      //       })
    
      //       this.multi_line = totalBillingLineGraph;
    
      //       // table display
    
      //       this.billingList = [];
      //       result.data.forEach(element => {
      //         let totalArrayData = {
      //           email: element.userEmail,
      //           account: element.invoiceId,
      //           amount: element.total,
      //           date: this.service.formattedDate(element.createdAt),
      //           currency: element.planCurrency,
      //           paid: element.isPaid,
      //           status: element.status,
      //           id: element._id
      //         }
      //         this.billingList.push(totalArrayData);
      //       });
      //       console.log(this.billingList);
      //       this.rows = this.billingList;
      //       this.temp = [...this.billingList];

      //       this.showView = true;
      //     }
      //   },(error)=>{
      //     if(error.status == 0){
      //       this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
      //     }
      //     console.log(error);
      //   })
      // }
  }

  viewFile(modalElement, data){
    // if(modalElement){
    //   let data = {
    //     domain: domainname
    //   }
    //   this.service.postConfig(this.getDateListApi, data)
    //   .subscribe((data: any) => {
    //     if(data != null || data != undefined){
    //       let dateArray = [];
    //       data.forEach(value => {
    //         dateArray.push({domainDate: this.service.formattedDate(value)});
    //       });
    //       this.modalService.open(modalElement, { size: 'lg' });
    //       this.modalSubTitle = domainname;
    //       this.rows2 = dateArray;
    //     }else{
    //       console.log("Not Valid");
    //     }
    //   },(error)=>{
    //     if(error.status == 0){
    //       this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
    //     }
    //     console.log(error);
    //   });
    // }
  }


    //search filter
    updateFilter(event) {
      const val = event.target.value.toLowerCase();
  
      // filter our data
      const temp = this.temp.filter(function(d) {
        return d.email.toLowerCase().indexOf(val) !== -1 || !val;
      });
  
      // update the rows
      this.billingList = temp;
      // Whenever the filter changes, always go back to the first page
      this.table = this.rows;
    }

}
