import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// admin pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { BillingComponent } from './billing/billing.component';
import { AccountsComponent } from './accounts/accounts.component';
import { InboxComponent } from './inbox/inbox.component';
import { PlanComponent } from './plan/plan.component';
import { PlanPriceComponent } from './plan-price/plan-price.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      title: 'Dashboard',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Dashboard' },
      ],
      role: 'admin'
    }
  },
  {
    path:'subscriptions',
    component:SubscriptionsComponent,
    data: {
      title: 'Subscriptions',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Subscriptions' }
      ]
    }
  },
  {
    path:'billing',
    component:BillingComponent,
    data: {
      title: 'Billing',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Billing' },
      ],
      role: 'admin'
    }
  },
  {
    path:'settings',
    component:AccountsComponent,
    data: {
      title: 'Settings',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Settings' },
      ],
      role: 'admin'
    }
  },
  {
    path:'plans',
    component:PlanComponent,
    data: {
      title: 'Plans',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Settings', url: '/settings' },
        { title: 'Plans' },
      ],
      role: 'admin'
    }
  },
  {
    path:'plan-price',
    component:PlanPriceComponent,
    data: {
      title: 'Plan Price',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Settings', url: '/settings' },
        { title: 'Plan Price' },
      ],
      role: 'admin'
    }
  },
  {
    path:'inbox',
    component:InboxComponent,
    data: {
      title: 'Inbox',
      urls: [
        { title: 'Home', url: '/dashboard' },
        { title: 'Inbox' },
      ],
      role: 'admin'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
