import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';

// admin pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';
import { BillingComponent } from './billing/billing.component';
import { AccountsComponent } from './accounts/accounts.component';
import { InboxComponent } from './inbox/inbox.component';
import { PlanComponent } from './plan/plan.component';
import { PlanPriceComponent } from './plan-price/plan-price.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { UiSwitchModule } from 'ngx-ui-switch';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartsModule } from 'ng2-charts';
// Import your library
// import { NgxStripeModule } from 'ngx-stripe';
@NgModule({
  declarations: [DashboardComponent, SubscriptionsComponent, BillingComponent, AccountsComponent, InboxComponent, PlanComponent, PlanPriceComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,FormsModule, ReactiveFormsModule,ToastrModule, UiSwitchModule, NgxDatatableModule,ChartsModule,  BsDatepickerModule.forRoot()
    // NgxStripeModule.forRoot(),
  ],
  providers: []
})
export class AdminModule { }
