import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalServiceService } from '../../shared/global-service.service';
import { FormControl, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {
  changePasswordForm: FormGroup;
  profileForm: FormGroup;
  profileApi = 'profile';
  image = null;
  CountryList  = [];
  GenderList = [];
  targetFile:any;
  uploadFile:any;
  uploadFileName:string = "Upload Avatar";
  changePasswordApi: string = "profile/changepassword";
  getCountriesApi:string = "country/all";
  minAgeRequired:number = 16;
  // ================================================================
  // VALIDATION MESSAGES
  validation_messages = {
    'old_password': [
      { type: 'required', message: 'Current Password is required.' }
    ],
    'new_password': [
      { type: 'required', message: 'New Password is required.' },
      { type: 'pattern', message: 'New Password containing at least 8 characters, 1 number,1 special character, 1 upper and 1 lowercase.' }
    ],
    'first_name': [
      { type: 'required', message: 'First Name is required.' }
    ],
    'last_name': [
      { type: 'required', message: 'Last Name is required.' }
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Email not Valid' }
    ],
    'phone_number': [
      { type: 'required', message: 'Phone Number is required.' }
    ],
    'date_of_birth': [
      { type: 'required', message: 'Date Of Birth is required.' }
    ],
    'gender_id': [
      { type: 'required', message: 'Gender is required.' }
    ],
    'company': [
      { type: 'required', message: 'Company is required.' }
    ],
    'position': [
      { type: 'required', message: 'Position is required.' }
    ],
    'address_1': [
      { type: 'required', message: 'Address 1 is required.' }
    ],
    'address_2': [
      { type: 'required', message: 'Address 2 is required.' }
    ],
    'city': [
      { type: 'required', message: 'City is required.' }
    ],
    'state': [
      { type: 'required', message: 'State is required.' }
    ],
    'country': [
      { type: 'required', message: 'Country is required.' }
    ],
    'postal_code': [
      { type: 'required', message: 'Postal Code is required.' }
    ]

  };

  constructor(public fb: FormBuilder, public service: GlobalServiceService, public toastr: ToastrService, private router: Router) {
    this.image = this.service.placeholderImage + "?text=Profile";
  }

  ngOnInit() {
    this.formInit();
    this.profileFormInit();
    this.getAllCountries();
    this.CountryList = [{
      id: 1,
      countryName: "Canada"
    }, {
      id: 2,
      countryName: "America"
    }, {
      id: 3,
      countryName: "India"
    }];
    this.GenderList = [{
      id: 1,
      gender: "Male",
      value: 'male'
    },
    {
      id: 2,
      gender: "Female",
      value: 'female'
    },
    {
      id: 3,
      gender: "Prefer Not to Answer",
      value: 'na'
    }];
  }

  // =========================================================================================================
  // FORM INITIATE
  // ==========================
  formInit = () => {
    this.changePasswordForm = this.fb.group({
      old_password: new FormControl(null, Validators.required),
      new_password: new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)])
    });
  }
  profileFormInit = () => {
    this.profileForm = this.fb.group({
      id: new FormControl(null),
      profile_photo: new FormControl(null),
      first_name: new FormControl(null, Validators.required),
      last_name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/)]),
      phone_number: new FormControl(null, Validators.required),
      date_of_birth: new FormControl(null, Validators.required),
      gender_id: new FormControl(null, Validators.required),
      company: new FormControl(null, Validators.required),
      position: new FormControl(null, Validators.required),
      address_1: new FormControl(null, Validators.required),
      address_2: new FormControl(null),
      city: new FormControl(null, Validators.required),
      state: new FormControl(null, Validators.required),
      country: new FormControl(null, Validators.required),
      postal_code: new FormControl(null, Validators.required)
    });
  }
  // =======================================================================================================
  changePassword = () => {
    if (this.changePasswordForm.valid) {
      if (this.changePasswordForm.value.new_password != this.changePasswordForm.value.old_password) {
        let data = {
          oldPassword: this.changePasswordForm.value.old_password,
          newPassword: this.changePasswordForm.value.new_password
        }
  
        this.service.post_data(this.changePasswordApi, data)
        .subscribe((data: any) => {
          console.log(data);
          if(data.msg == "PASSWORD_CHANGED"){
            this.changePasswordForm.reset();
            this.toastr.success( 'Password Updated Successfully');
          }
        },(error)=>{
          if(error.error.errors.msg == "WRONG_PASSWORD"){
            this.toastr.error('Please Use Different Password');
          }else if(error.status == 0){
            this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
          }
          console.log(error);
        });
      } else {
        this.toastr.error('New Password and old Password is Same Please Change It');
      }
    } else {
      this.service.validateAllFormControl(this.changePasswordForm)
    }
  }

  onProfileFormSubmit() {
    console.log(this.profileForm.value)
    if (this.profileForm.valid) {
      if(this.service.calculateAge(this.profileForm.value.date_of_birth) > this.minAgeRequired){
      this.service.changeMessage("fileUpload"); // for changing the header enctype
      const formData: FormData = new FormData();
      if(this.targetFile){
      formData.append('avatar', this.targetFile.files[0], this.targetFile.files[0].name);
      }
      formData.append('firstname', this.profileForm.value.first_name);
      formData.append('lastname', this.profileForm.value.last_name);
      formData.append('email', this.profileForm.value.email);
      formData.append('phone', this.profileForm.value.phone_number);
      formData.append('company', this.profileForm.value.company);
      formData.append('position', this.profileForm.value.position);
      formData.append('address1', this.profileForm.value.address_1);
      formData.append('address2', this.profileForm.value.address_2);
      formData.append('city', this.profileForm.value.city);
      formData.append('state', this.profileForm.value.state);
      formData.append('postalcode', this.profileForm.value.postal_code);
      formData.append('country',  this.profileForm.value.country);
      formData.append('gender', this.profileForm.value.gender_id);
      formData.append('dob', this.profileForm.value.date_of_birth);
      // console.log(this.service.toFormData(obj));
      this.service.patchAuthentication(this.profileApi, formData).subscribe((result) => {
        console.log(result);
        if(result != null || result != undefined){
          this.toastr.success( 'Profile Updated Successfully');
          this.service.changeMessage("profile_updated");
        }
      }, (error)=>{
        if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
            console.log(error);
      });
    }else{
      this.toastr.error( 'Your Current Age is lesser than 16!');
    }
    }else {
      this.service.validateAllFormControl(this.profileForm)
    }
  }
  // =========================================================================================================
  // GET User
  // ===========================
  getProfileData = () => {
    this.service.get_lists(this.profileApi)
    .subscribe((data: any) => {
      if(data != null || data != undefined){
        console.log(data);   
        this.profileForm.patchValue({
          first_name: data.firstname,
          last_name: data.lastname,
          email: data.email,
          phone_number: data.phone,
          gender_id: data.gender,
          company: data.company,
          position: data.position,
          address_1: data.address1,
          address_2: (data.address2 === ('undefined' && 'null'))? null: data.address2,
          city: data.city,
          state: data.state,
          country: data.country,
          postal_code: data.postalcode
        })
        let date = new Date(data.dob);
        this.profileForm.get('date_of_birth').setValue({
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()
        });
        this.image = data.avatar;
    }else{
      console.log('not valid');
    }
    },(error)=>{
      if(error.status == 0){
        this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
      }
      console.log(error);
    });
  }

  // =========================================================================================================
  // FORM UPLOAD
  // ========================
  onFileChange(event: any) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        // this.profileForm.get('profile_photo').setValue(reader.result);
        this.targetFile=event.target;
        this.uploadFileName=this.targetFile.files[0].name;
        console.log(reader)
        this.image = reader.result;
      };
    }
  }
  // ===============================================================================================================================

  getAllCountries(){
    this.service.getAuthentication(this.getCountriesApi).subscribe((result) => {
      if(result != null || result != undefined){
      this.CountryList = [];
      console.log(result);
      result.forEach(element => {
        let totalArrayData = {
          countryName: element.name,
          id: element._id,
          code: element.code
        }
        this.CountryList.push(totalArrayData);
      });
      this.getProfileData();
      }
    })
  }
}
