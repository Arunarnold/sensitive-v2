import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalServiceService } from '../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';

declare var require: any;
const data: any = require('./dashboard.json');
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  host: {
    '(document:resize)': 'onResize($event)'
  }
})
export class DashboardComponent implements OnInit {
  dashboardStatsApi:string = "admin/dashboard";
  notifications:number = 0;
  subscriptions:number = 0;
  packages:number = 0;
  dormant:number = 0;


    // This is for the dashboar line chart
  // lineChart
  public lineChartData: Array<any> = [
    { data: [50, 130, 80, 70, 180, 105, 250],label: 'Profanity', lineTension: 0},
    { data: [80, 100, 60, 200, 150, 100, 150],label: 'Total API Calls', lineTension:0}
  ];

  public lineChartLabels: Array<any> = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July'
  ];
  public lineChartOptions: any = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          },
          gridLines: {
            color: 'rgba(120, 130, 140, 0.13)'
          }
        }
      ],
      xAxes: [
        {
          gridLines: {
            color: 'rgba(120, 130, 140, 0.13)'
          }
        }
      ]
    },
    lineTension: 0,
    responsive: true,
    maintainAspectRatio: false
  };
  public lineChartColors: Array<any> = [
    {
      // grey
      backgroundColor: 'rgba(25,118,210,0.0)',
      borderColor: 'rgba(25,118,210,1)',
      pointBackgroundColor: 'rgba(25,118,210,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(25,118,210,0.5)'
    },
    {
      // dark grey
      backgroundColor: 'rgba(38,218,210,0.0)',
      borderColor: 'rgba(38,218,210,1)',
      pointBackgroundColor: 'rgba(38,218,210,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(38,218,210,0.5)'
    }
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';


  rows:any = [];
  temp:any = [];
  loginList = [];
  selectPageValue: number = 10;
  @ViewChild(DashboardComponent, {static: true}) table: DashboardComponent | undefined;
  constructor( public service: GlobalServiceService, public toastr: ToastrService) {
    this.loginList = data;
    this.rows = this.loginList;
    this.temp = [...this.rows];
  }

  ngOnInit(): void {
    this.getDashboardStats();
  }



  onResize(event) {
    console.log(event)
    // this.view1 = [event.target.innerWidth / 1.35, 400];
  }

  getDashboardStats() {
    // this.service.get_lists(this.dashboardStatsApi).subscribe((result) => {
    //   console.log(result);
    //   if(result != null || result != undefined){
    //     this.notifications = result.notifications;
    //     this.subscriptions = result.subscriptions;
    //     this.packages = result.packages;
    //     this.dormant = result.dormant;
    //     this.subscriber = [
    //       {
    //         "name": "Subscribers",
    //         "value": result.subscriptions || 0
    //       },
    //       {
    //         "name": "Non-Subscribers",
    //         "value": result.dormant || 0
    //       }
    //     ];
      
    //     this.gold = [
    //       {
    //         "name": "Gold",
    //         "value": result.gold || 0
    //       },
    //       {
    //         "name": "Other",
    //         "value": (result.basic + result.silver) || 0
    //       }
    //     ];
    //     this.basic = [
    //       {
    //         "name": "Basic",
    //         "value": result.basic || 0
    //       },
    //       {
    //         "name": "Other",
    //         "value": (result.gold + result.silver) || 0
    //       }
    //     ];
    //     this.silver = [
    //       {
    //         "name": "Silver",
    //         "value": result.silver || 0
    //       },
    //       {
    //         "name": "Other",
    //         "value": (result.basic + result.gold) || 0
    //       }
    //     ];
      

    //     //bar chart

    //     let barGraph = [];
    //     let totalBarGraph = [];
    //     this.multi  = [];
    //     result.subscriptionsBar.forEach(element => {
    //       let totalArrayData = {
    //         name: element.date,
    //         value: element.count,
    //       }
    //       barGraph.push(totalArrayData)
    //     });
    //     console.log(barGraph);

    //     result.subscriptionsBar.forEach(element => {
    //       let totalArrayData = {
    //         name: this.service.formattedYear(element.date),
    //         series: barGraph,
    //       }
    //       totalBarGraph.push(totalArrayData);
    //     })

    //     this.multi = barGraph;

    //     // line chart
    //     let lineGraph = [];
    //     let totalLineGraph = [];
    //     this.multi_line  = [];
    //     result.subscriptionsLine.forEach(element => {
    //       let totalArrayData = {
    //         name: element.date,
    //         value: element.count,
    //       }
    //       lineGraph.push(totalArrayData)
    //     });
    //     console.log(lineGraph);

    //     result.subscriptionsLine.forEach(element => {
    //       let totalArrayData = {
    //         name: this.service.formattedYear(element.date),
    //         series: lineGraph,
    //       }
    //       totalLineGraph.push(totalArrayData);
    //     })

    //     this.multi_line = totalLineGraph;

    //   }
    //   },(error)=>{
    //     if(error.status == 0){
    //       this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
    //     }
    //     console.log(error);
    //   });
  }

  //search filter
  updateFilter(event:any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d:any) {
      return d.ipAddress.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.loginList = temp;
    // Whenever the filter changes, always go back to the first page
    this.table = this.rows;
  }
}