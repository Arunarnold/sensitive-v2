import { Component, OnInit, AfterViewInit, HostListener, ElementRef, ViewEncapsulation, ViewChild} from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { InvisibleReCaptchaComponent  } from 'ngx-captcha';
import { FormControl, FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { GlobalServiceService } from '../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';
// declare var mixitup: any;
declare var tns: any;
declare var WOW: any;
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomePageComponent implements OnInit,AfterViewInit {
  topPosToStartShowing:any = 100;
  isShow: boolean;
  getheaderIds:any;
  getId:any = "wrapper";
  contactForm: FormGroup;
  contactFormApi:string = "message/contact";
  showNumber: any = {
    number1: 0,
    number2: 0,
    number3: 0
  }
  myOpts =  {
    duration : 2
  };
  progressValue:any = {
    value1: 0,
    value2: 0,
    value3: 0,
    value4: 0
  };

  priceSwitch: boolean = false;
  planPriceListApi = "price/all";
  faqListApi:string = "faq";
  activeId: string = "faq-panel-0";
  choosePlan: string = "BizBudget-Gold";
  priceInterval:string = "month";
  planPriceList = [
    {
      planName: "Basic",
      price: "5.99",
      currency: "USD",
      interval: "month",
      status: "active",
      stripeId: "123456",
      isFeatured: false,
      planOrder: 1,
      planDescription: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
      id: "123456789"
    },
    {
      planName: "Gold",
      price: "12.99",
      currency: "USD",
      interval: "month",
      status: "active",
      stripeId: "123456",
      isFeatured: false,
      planOrder: 3,
      planDescription: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
      id: "123456789"
    },
    {
      planName: "Silver",
      price: "29.99",
      currency: "USD",
      interval: "month",
      status: "active",
      stripeId: "123456",
      isFeatured: true,
      planOrder: 2,
      planDescription: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
      id: "123456789"
    },
    {
      planName: "Gold",
      price: "59.99",
      currency: "USD",
      interval: "year",
      status: "active",
      stripeId: "123456",
      isFeatured: true,
      planOrder: 3,
      planDescription: ["Feature 1", "Feature 2", "Feature 3", "Feature 4"],
      id: "123456789"
    }
  ];
  showPlans= [];

  faqList = [
    {
      title: "What will happen if my limit on free edition exceeds?",
      description: "A mail will be sent stating about exceed in limit, therefore user can subscribe and enjoy the service again"
    },
    {
      title: "What is the actual subscriber limit when I have multiple websites?",
      description: "Subscribers are counted overall for all the existing websites and not separately."
    },
    {
      title: "How can I track my usage?",
      description: "In your dashboard you can track, among other things, the number of open connections and the number of messages per day."
    },
    {
      title: "I have another question, who do I ask?",
      description: "Please search if your question is covered in our Knowledge Base. If you don't find you answer there, please mail us at support@acestranetworks.com"
    },
    {
      title: "Can I downgrade my plan?",
      description: "Yes, you can downgrade your plan at any time. However you can't send notifications if the limit is lower than the number of users that you currently have."
    },
    {
      title: "How do i upgrade to another package?",
      description: "This can be done by logging in and and change planns in accounts panel"
    }
  ];
  siteKey:string;
  captchaIsLoaded = false;
  captchaSuccess = false;
  captchaResponse?: string;
  captchaIsReady = false;
  badge: 'bottomright' | 'bottomleft' | 'inline' = 'bottomright';
  type: 'image' | 'audio' = 'image';
  theme: 'light' | 'dark' = 'light';
  recaptcha: any = null;
  @ViewChild('captchaContactUsForm', { static: false }) captchaElem: InvisibleReCaptchaComponent;
  constructor(private service:GlobalServiceService, private router : Router, private elRef : ElementRef,public fb: FormBuilder, public toastr: ToastrService) {
    this.showPlans = this.planPriceList.sort((a:any,b:any) => a.planOrder - b.planOrder).filter( (e) => {
      return e.interval == this.priceInterval;
  });
  }
  
  scroll(el) {
    console.log(el);
    if(el != null){

    document.getElementById(el).scrollIntoView({ behavior: 'smooth' });
    }
  }

  sticky = false;

  @HostListener('window:scroll', ['$event'])
  ngOnInit() {

    if ('scrollRestoration' in history) {
        history.scrollRestoration = 'manual';
    }
    // This is needed if the user scrolls down during page load and you want to make sure the page is scrolled to the top once it's fully loaded. This has Cross-browser support.
    window.scrollTo(0,0);


    // let mixitupGrid = this.elRef.nativeElement.querySelector(".zerogrid");
    // mixitup(mixitupGrid);
    tns({
      container: '.app_screens',
      items: 5,
      mouseDrag: true,
      autoplay: true,
      nav: true,
      navPosition: 'bottom',
      gutter: 35,
      edgePadding:5,
      controls: false,
      loop: true,
      autoplayButtonOutput: false
    });

    tns({
      container: '.client-slider',
      items:5,
      mouseDrag: true,
      autoplay: true,
      nav: false,
      gutter: 5,
      edgePadding:5,
      controls: false,
      loop: true,
      autoplayButtonOutput: false
    });

    tns({
      container: '.app_carousel',
      items:1,
      mouseDrag: true,
      autoplay: true,
      nav: false,
      gutter: 5,
      edgePadding:5,
      controls: false,
      loop: true,
      autoplayButtonOutput: false
    });
    tns({
      container: '.testimonial_carousel',
      items:1,
      mouseDrag: true,
      autoplay: true,
      nav: true,
      navPosition: 'bottom',
      gutter: 5,
      edgePadding:5,
      controls: false,
      loop: true,
      autoplayButtonOutput: false
    });
  //  let wow = new WOW(
  //     {
  //     boxClass:     'wow',      // default
  //     animateClass: 'animated', // default
  //     offset:       0,          // default
  //     mobile:       false,       // default
  //     live:         true        // default
  //   }
  //   )
  //   wow.init();

    this.getplanPriceList();
    this.getFaqList();
    this.contactFormInit();
  }

  ngAfterViewInit() {
    window.resizeBy(100, 100);
    setTimeout(() => {
      this.captchaIsLoaded = true;
      if(this.captchaIsLoaded){
          this.siteKey = this.service.recaptchaSitekey; 
      }
    }, 100);
  }

  
  @HostListener('window:scroll', ['$event'])
  checkScroll(event) {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
    let headerid:any = this.getheaderIds;
    headerid.forEach(element => {
    if(document.getElementById(element) != null){
      let scrolling = document.getElementById(element).getBoundingClientRect();
      if(scrolling.y < 10 && scrolling.y > -120){
        this.getdata(element)
      }
    }
    });

  }
  
  getdata(data){

    this.getId = data;
  }

  goToSignUp(planType, priceId){

    const navigationExtras: NavigationExtras = {
      state: {
        plan: planType,
        priceId: priceId
      }
    };
    this.router.navigate(['authentication/signup'], navigationExtras);
  }

  scrollToTop() {
      window.scroll({ 
        top: 0, 
        left: 0, 
        behavior: 'smooth' 
      });
    }

    getheaderId(data){
      this.getheaderIds = data;
    }


    selectedInterval(event){
      if(event == true){
        this.priceInterval = 'year';
      }else{
        this.priceInterval = 'month';
      }
      this.showPlans = this.planPriceList.sort((a:any,b:any) => a.planOrder - b.planOrder).filter( (e) => {
        return e.interval == this.priceInterval;
    });
    }
  
    getplanPriceList() {
      this.service.website_get_lists(this.planPriceListApi).subscribe((result) => {
        console.log(result);
        if(result != null || result != undefined){
          this.planPriceList = [];
          result.forEach(element => {
            let totalArrayData = {
              planName: element.planName,
              price: element.planPrice,
              currency: element.currency,
              interval: element.priceInterval,
              status: element.status,
              stripeId: element.priceStripeId,
              isFeatured: element.isFeatured,
              planOrder: element.planOrder,
              planDescription: this.service.stringSplitterByComma((element.planDescription)?element.planDescription : 'Currently No plans' ),
              id: element.priceId
            }
            this.planPriceList.push(totalArrayData);
            console.log(totalArrayData)
          
          });
          console.log(this.planPriceList);
         this.showPlans = this.planPriceList.sort((a:any,b:any) => a.planOrder - b.planOrder).filter( (e) => {
            return e.interval == this.priceInterval;
        });
        }
        });
    }

    toggleAccordian(event) {
      // If it is already open you will close it and if it is closed open it
      this.activeId = this.activeId == event.panelId ? "" : event.panelId;
    }

    getFaqList(){
      this.service.website_get_lists(this.faqListApi).subscribe((result) => {
        console.log(result)
        if(result != null || result != undefined){
          this.faqList = [];
          result.forEach(element => {
            let totalArrayData = {
              title: element.question,
              description: element.answer
            }
            this.faqList.push(totalArrayData);
          });
        }
      })
    }


    contactFormInit(){
      this.contactForm = this.fb.group({
        name: new FormControl(null, Validators.required),
        subject: new FormControl(null, Validators.required),
        message: new FormControl(null, Validators.required),
        email: new FormControl(null, [Validators.required, Validators.email])
      });
    }
    handleLoad(): void {
      this.captchaIsLoaded = true;
    }
  
    handleReady(): void {
      this.captchaIsReady = true;
    }
  
    handleExpire(){
      console.log("Recaptcha Expired");
      this.reload();
    }
  
    reload(): void {
      this.captchaElem.reloadCaptcha();
    }
  
    handleReset(){
      console.log("Recaptcha Reset");
      this.reset();
    }
    
    handleError(){
      
    }
    reset(): void {
      this.captchaElem.resetCaptcha();
    }
    submitContactForm(){
      console.log(this.contactForm.value)

      if (this.contactForm.valid) {
         this.captchaElem.execute();
  
      } else {
        this.service.validateAllFormControl(this.contactForm);
  
      }
    }

handleSuccess(captchaResponse: string) {
  this.captchaSuccess = true;
  this.captchaResponse = captchaResponse;
  if(this.captchaResponse){
  console.log(this.contactForm.value);
    if (this.contactForm.valid) {
      let postData = {
        name: this.contactForm.value.category_name,
        subject: this.contactForm.value.subject,
        message : this.contactForm.value.message,
        email: this.contactForm.value.email,
        recaptcha: this.captchaResponse
      }
      this.service.website_post_data(this.contactFormApi, postData).subscribe((result) => {
        if(result){
          this.toastr.success("Contact Send Successfully", this.contactForm.value.category_name);
          this.resetForm();
        }
      },(error)=>{
        if(error.status == 422){
          let errorMessage = error.error.errors.msg;
          errorMessage.forEach(element => {
            this.toastr.error(element.msg, element.param);  
          });
        }else if(error.status == 0){
          this.toastr.error('Please Check The Internet Connectivity', 'Something Went Wrong');
        }
        console.log(error);
      })
  }else{
    this.service.validateAllFormControl(this.contactForm);
  }
}else{
  console.log("recaptcha not found");
}
}

resetForm() {
  this.contactForm.reset();
}
}
