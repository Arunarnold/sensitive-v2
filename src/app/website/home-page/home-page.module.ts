import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { HomePageRoutingModule } from './home-page-routing.module';
import { HomePageComponent } from './home-page.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { WebsiteModule } from './../../website/website/website.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgxCaptchaModule } from 'ngx-captcha';
@NgModule({
  declarations: [HomePageComponent],
  imports: [CommonModule, HomePageRoutingModule,
    WebsiteModule,UiSwitchModule,FormsModule, ReactiveFormsModule, ToastrModule.forRoot(), NgxCaptchaModule,ProgressbarModule.forRoot()],
})
export class HomePageModule {}
