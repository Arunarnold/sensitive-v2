import { Component, OnInit, Output, EventEmitter,HostListener, Input, SimpleChanges } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { GlobalServiceService } from '../../../shared/global-service.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],

})
export class HeaderComponent implements OnInit {
  sticky = false;
  icon_bar_btn = false;
  toggle = false;
  bgClass: any;
  public isMenuCollapsed = true;
  @Output() scroll_name = new EventEmitter;
  @Output() headerIds = new EventEmitter;
  @Input() getId:any;
  currentSection:any;
  logoutAuthApi:string = "logout";
  showLogoutButton:boolean;
  // bgClass = 'default';
  constructor(private router: Router, private service: GlobalServiceService, public toastr: ToastrService) {// subscribe to router navigation
    // this.router.events.subscribe(event => {
      // filter `NavigationEnd` events
      // if (event instanceof NavigationEnd) {
      //   // get current route without leading slash `/`
      //   const eventUrl = /(?<=\/).+/.exec(event.urlAfterRedirects);
      //   const currentRoute = (eventUrl || []).join('');
      //   // set bgClass property with the value of the current route
      //   this.bgClass = currentRoute.split('/')[0];
      //   console.log(this.bgClass)
      // }
    // });
  }
  scroll(el) {
    this.scroll_name.emit(el);
  }

  ngOnInit(): void {
    let getAllNavIds=[];
    let element:any = document.querySelector('#navbarList').childNodes;
    element.forEach( element => {
      if(element.dataset.navId){
      getAllNavIds.push(element.dataset.navId);
      }
    });
    this.headerIds.emit(getAllNavIds);
    this.currentSection = this.getId;
    if(localStorage.getItem('loginID') && localStorage.getItem('token') && JSON.parse(localStorage.getItem('loginID')).role){
      if(JSON.parse(localStorage.getItem('loginID')).role == 'admin'|| JSON.parse(localStorage.getItem('loginID')).role == 'user'){
        this.showLogoutButton = true;
      }
    }else{
      this.showLogoutButton = false;
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    if(changes.getId.currentValue){
      this.currentSection = changes.getId.currentValue;
    }
    // this.doSomething(changes.categoryId.currentValue);
    // You can also use categoryId.previousValue and 
    // categoryId.firstChange for comparing old and new values

}
  @HostListener('window:scroll')
  affixScroll(){
            let scrollpos = window.scrollY
        const header:any = document.querySelector(".header-holder");
        const header_height = header.offsetHeight
      
        const add_class_on_scroll = () => header.classList.add("stricked-menu","stricky-fixed")
        const remove_class_on_scroll = () => header.classList.remove("stricked-menu","stricky-fixed")
      
        window.addEventListener('scroll', function() { 
          scrollpos = window.scrollY;
      
          if (scrollpos >= header_height) { add_class_on_scroll() }
          else { remove_class_on_scroll();}
    
        })
  }


  logoutButton(){
    // if(localStorage.getItem('loginID') && localStorage.getItem('token') && JSON.parse(localStorage.getItem('loginID')).role){
    //   if(JSON.parse(localStorage.getItem('loginID')).role == 'admin'|| JSON.parse(localStorage.getItem('loginID')).role == 'user'){
    //     this.service.getAuthentication(this.logoutAuthApi)
    //     .subscribe((data: any) => {
    //       if(data.message == "logout"){
    //           localStorage.removeItem('loginID');
    //           localStorage.removeItem('loggedIntime');
    //           let dataMessage = {
    //             message : "loginID",
    //             token: ''
    //           }
    //           this.service.changeMessage(dataMessage);
    //           this.showLogoutButton = false;
    //         this.toastr.success("Logged Out Successfully");
    //       }
    //     })
    //   }
    // }

    if(localStorage.getItem('loginID') && localStorage.getItem('token') && JSON.parse(localStorage.getItem('loginID')).role){
      localStorage.removeItem('loginID');
      localStorage.removeItem('token');
      this.router.navigate(['/authentication/login']);
    }
  }


  gotoDashboard(){
    if(localStorage.getItem('loginID') && localStorage.getItem('token') && JSON.parse(localStorage.getItem('loginID')).role){
      if( JSON.parse(localStorage.getItem('loginID')).role == 'admin'){
        this.router.navigateByUrl('/admin/dashboard',{replaceUrl: true});
      }else if( JSON.parse(localStorage.getItem('loginID')).role == 'user'){
        this.router.navigateByUrl('/dashboard_user',{replaceUrl: true})
      }
    }
  }
  
}
