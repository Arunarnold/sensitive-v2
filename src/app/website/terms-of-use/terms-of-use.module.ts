import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsofUseRoutingModule } from './terms-of-use-routing.module';
import { TermsofUseComponent } from './terms-of-use.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { WebsiteModule } from '../../website/website/website.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [TermsofUseComponent],
  imports: [CommonModule, TermsofUseRoutingModule,WebsiteModule,UiSwitchModule,FormsModule, ReactiveFormsModule, ToastrModule.forRoot()],
})
export class TermsofUseModule {}
