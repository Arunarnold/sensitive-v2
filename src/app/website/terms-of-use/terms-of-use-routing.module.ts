import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TermsofUseComponent } from '../terms-of-use/terms-of-use.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'terms-of-use',
    pathMatch: 'full',
  },
  {
    path: '',
    component: TermsofUseComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TermsofUseRoutingModule { }
