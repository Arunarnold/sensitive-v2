import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrivacyRoutingModule } from './privacy-routing.module';
import { PrivacyComponent } from './privacy.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { WebsiteModule } from '../../website/website/website.module';
import { ToastrModule } from 'ngx-toastr';
@NgModule({
  declarations: [PrivacyComponent],
  imports: [CommonModule, PrivacyRoutingModule,WebsiteModule,UiSwitchModule,ToastrModule.forRoot()],
})
export class PrivacyModule {}
