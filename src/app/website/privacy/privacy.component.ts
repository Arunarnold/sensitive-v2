import { Component, OnInit,ViewEncapsulation, HostListener} from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./privacy.component.css']
})

export class PrivacyComponent implements OnInit {
  topPosToStartShowing:any = 100;
  isShow: boolean;
  getheaderIds:any;
  getId:any = "wrapper";
  constructor(private router: Router) {
  }
  @HostListener('window:scroll', ['$event'])
  ngOnInit() {


  if ('scrollRestoration' in history) {
    history.scrollRestoration = 'manual';
  }
  // This is needed if the user scrolls down during page load and you want to make sure the page is scrolled to the top once it's fully loaded. This has Cross-browser support.
  window.scrollTo(0,0);
  }


  @HostListener('window:scroll', ['$event'])
  checkScroll(event) {
    const scrollPosition = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
    let headerid:any = this.getheaderIds;
    headerid.forEach(element => {
    if(document.getElementById(element) != null){
      let scrolling = document.getElementById(element).getBoundingClientRect();
      if(scrolling.y < 10 && scrolling.y > -120){
        this.getdata(element)
      }
    }
    });
  }

  async scroll(el) {
    if(el != null){
     await this.router.navigate(["home"]).then((data)=>{
        if(data == true){
          setTimeout(() => {
            document.getElementById(el).scrollIntoView({ behavior: 'smooth' });
          }, 800);
        }
      });
    }
  }

  getdata(data){
    this.getId = data;
  }

  scrollToTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }

  getheaderId(data){
    this.getheaderIds = data;
  }
}
