import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders , HttpErrorResponse} from '@angular/common/http';
import { FormControl, FormGroup} from '@angular/forms';
import { environment } from '../../environments/environment.prod';
import { BehaviorSubject, Subject, throwError } from 'rxjs';
import { catchError,map } from 'rxjs/operators'
// import {
//   StripeCardElementOptions,
//   StripeElementsOptions,
//   PaymentIntent,
// } from '@stripe/stripe-js';
export const googleAuth = {
  googleAuthProviderID : environment.googleAuthProviderID
}
export const facebookAuth = {
  facebookAuthProviderID : environment.facebookAuthProviderID
}

@Injectable({
  providedIn: 'root'
})
export class GlobalServiceService {

  API_URL = environment.api_url;

  authenticationUrl = environment.api_url; //no sub url like api/
  recaptchaSitekey = environment.recaptchaSitekey;
  public static WEBSITE_API_URL = environment.websiteApiUrl;
  // public API_IMAGE_URL = environment.image_url;
  placeholderImage = environment.placeHolderImage;
  setMinutesForRefreshToken = environment.setMinutesForRefreshToken;
  headers: any;
  emitChangeSource = new Subject<Observable<any>>();
  
  private messageSource = new BehaviorSubject('');
  currentMessage = this.messageSource.asObservable();
  private messageSource2 = new BehaviorSubject('');
  currentMessage2 = this.messageSource2.asObservable();
  constructor(private http: HttpClient) {

    // this.headers.append('Content-Type', 'application/json');
    if(localStorage.getItem('token')){
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token'))
      });
    }else{
      this.headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
    }
  }

  changeMessage(message: any) {
    this.messageSource.next(message);
    if(message == "fileUpload"){
      if(localStorage.getItem('token')){
      this.headers = new HttpHeaders({
        'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token'))
      }); 
    }else{
      this.headers = new HttpHeaders({
        'enctype': 'multipart/form-data'
      }); 
    }
    }else if(message.message == "loginID"){
      localStorage.setItem('token', JSON.stringify(message.token));
      if(localStorage.getItem('token')){
        this.headers = new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + JSON.parse(localStorage.getItem('token'))
        });
      }
    }
  }
  changeMessage2(message: any) {
    this.messageSource2.next(message);
  }

  website_post_data(particle_url, obj): Observable<any> {
    return this.http.post(GlobalServiceService.WEBSITE_API_URL + particle_url, obj, {headers: this.headers});
  }
  website_get_lists(particle_url): Observable<any> {
    return this.http.get(GlobalServiceService.WEBSITE_API_URL + particle_url, {headers: this.headers});
  }
  
  post_data(particle_url, obj): Observable<any> {
    return this.http.post(this.API_URL + particle_url, obj, {headers: this.headers}).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  get_lists(particle_url): Observable<any> {
    return this.http.get(this.API_URL + particle_url,{headers: this.headers}).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  get_data(particle_url, id): Observable<any> {
    return this.http.get(this.API_URL + particle_url + '/' + id, {headers: this.headers}).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  put_data(particle_url, id, obj): Observable<any> {
    const api_url = this.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    return this.http.patch(url, JSON.stringify(obj), { headers: this.headers }).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  delete_data(particle_url, id): Observable<any> {
    const api_url = this.API_URL + particle_url;
    const url = `${api_url}/${id}`;
    return this.http.delete(url, { headers: this.headers }).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  getAuthentication(particle_url): Observable<any> {
    return this.http.get(this.authenticationUrl+particle_url,{headers: this.headers}).pipe(
      map((res) => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }
  postAuthentication(particle_url: any, obj: any): Observable<any> {
    return this.http.post(this.authenticationUrl + particle_url,obj,{headers: this.headers}).pipe(
      map(res => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  patchAuthentication(particle_url: any, obj: any): Observable<any> {
    return this.http.patch(this.authenticationUrl + particle_url,obj,{headers: this.headers}).pipe(
      map(res => {
        if (!res) {
          throw new Error('Value expected!');
        }
        return res;
      }),
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse){
    return throwError(error);
    }

    // createPaymentIntent(api_url:any, obj:any): Observable<PaymentIntent> {
    //   return this.http.post<PaymentIntent>(
    //     this.API_URL + api_url,obj, {headers: this.headers}
    //   );
    // }
  //validate all forms
    validateAllFormControl(formName) {
      Object.keys(formName.controls).forEach(field => {  
        const control = formName.get(field);             
        if (control instanceof FormControl) {             
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof FormGroup) {        
          this.validateAllFormControl(control);            
        }
      });
    }

    // convert object to FormData 
    toFormData<T>( formValue: T ) {
      const formData = new FormData();
    
      for ( const key of Object.keys(formValue) ) {
        const value = formValue[key];
        formData.append(key, value);
      }
    
      return formData;
    }

    // convert Date format
    GetFormattedDate(date:string) {
      let getDate = new Date(date);
      let monthList = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
      let month = monthList[getDate.getMonth()];
      let day = getDate.getDate();
      let year = getDate.getFullYear();
      return  `${day}th ${month}, ${year}`;
    }
    
    //formatted date
    formattedDate(getDate){
      const date = new Date(getDate);
      const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
      const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(date);
      const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);
      return `${ye}-${mo}-${da}`;
    }

    //formatted Date Time
    formatDateTime(getDate) {
      const date = new Date(getDate);
      let hours = date.getHours();
      let minutes:any = date.getMinutes();
      let ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      let strTime = hours + ':' + minutes + ' ' + ampm;
      return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
    }
    
    //formatted year
    formattedYear(getYear){
      const date = new Date(getYear);
      const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
      return ye;
    }

    //split string separated by comma to array 
    stringSplitterByComma(data){
      return data.split(',');
    }

    //split string separated by dash 
    stringSplitterByDash(data){
      return data.split('-').pop();
    }
    
    //convert lowercase sting to Capitalize
    convertStringtoUcfirst(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
    }

    //only numbers for input
    validateInputasNumber(evt)
    {
        let CharValidate = new Array("08", "046", "039", "948", "235");
        let number_pressed = false;
        let Ncount;
        for (let i = 0; i < 5; i++)
        {
            for (Ncount = 0; Ncount < parseInt(CharValidate[i].substring(0, 1)) + 1; Ncount++)
            {
                if ((evt.keyCode || evt.which) == parseInt(CharValidate[i].substring(1, CharValidate[i].length)) + Ncount)
                {
                    number_pressed = true;
                }
            }
        }
        if (number_pressed == false)
        {
            evt.returnValue = false;
            if(evt.preventDefault){evt.preventDefault();}
        }
    }

    //calculate age
    calculateAge(dob) {
      if(dob != null){
        const d = new Date(dob.year, dob.month - 1, dob.day);
        const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('en', { month: 'numeric' }).format(d);
        const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        let calcualtedDate =  new Date(`${ye}-${mo}-${da}`);
        let ageDifMs = Date.now() - calcualtedDate.getTime();
        let ageDate = new Date(ageDifMs); // miliseconds from epoch
        let totalAge = Math.abs(ageDate.getUTCFullYear() - 1970);
        return totalAge
        }
    }

    //convertPricetoIndianFormat

    convertPricetoIndianFormat(data){
      let x = data.toString()
      let afterPoint = '';
      if(x.indexOf('.') > 0)
         afterPoint = x.substring(x.indexOf('.'),x.length);
      x = Math.floor(x);
      x=x.toString();
      let lastThree = x.substring(x.length-3);
      let otherNumbers = x.substring(0,x.length-3);
      if(otherNumbers != '')
          lastThree = ',' + lastThree;
      let res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
      return res;
    }
}
