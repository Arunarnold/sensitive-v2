import { Component, AfterViewInit, OnInit, Input } from '@angular/core';
import { userRoutes, adminRoutes } from './menu-items';
import { GlobalServiceService } from '../global-service.service';
import { RouteInfo } from './sidebar.metadata';
import { Router, ActivatedRoute } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {

  @Input() menuRole:string;
  dummyUser:any = {
    user:{
    planName : 'gold',
    fullname: 'user',
    emailId: 'user@user.com',
    },
    admin:{
     fullname: 'admin',
     emailId: 'admin@admin.com'
    }
 
  }
  planName:string;
  mainHeading:string;
  fullname:string;
  emailId:string;
  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems: any[];
  // this is for the open close
  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = '0';
    } else {
      this.showMenu = element;
    }
  }
  addActiveClass(element: any) {
    if (element === this.showSubMenu) {
      this.showSubMenu = '0';
    } else {
      this.showSubMenu = element;
    }
  }

  constructor(private service:GlobalServiceService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  // End open close
  ngOnInit() {
    this.getMenuList();
    if(this.menuRole == 'user'){
      this.planName  = this.dummyUser.user.planName;
      // this.paymentCheck();
    }
  }

  getMenuList = () => {
    if(this.menuRole == 'admin'){
      // admin dashboard menu
      this.sidebarnavItems = adminRoutes.filter(sidebarnavItem => sidebarnavItem);
      this.mainHeading = "Admin";
      this.fullname = this.dummyUser.admin.fullname;
      this.emailId = this.dummyUser.admin.emailId;
    }else if(this.menuRole == 'user'){
      // user dashboard menu
      this.sidebarnavItems = userRoutes.filter(sidebarnavItem => sidebarnavItem);
      this.mainHeading = "";
      this.fullname = this.dummyUser.user.fullname;
      this.emailId = this.dummyUser.user.emailId;
    }else{
      this.router.navigate(['/authentication/404'],{ replaceUrl: true });
    }
  }
  
}
