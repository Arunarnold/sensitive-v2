import { RouteInfo } from './sidebar.metadata';

export const userRoutes: RouteInfo[] = [
  {
    path: '/dashboard_user',
    title: 'Dashboard',
    icon: 'fa fa-home',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '',
    title: 'Word Filters',
    icon: 'fa fa-pencil-square-o',
    class: 'has-arrow',
    label: '2',
    labelClass: 'label label-rouded label-themecolor',
    extralink: false,
    submenu: [
      {
        path: '/word-list',
        title: 'Word List',
        icon: '',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
      },
      {
        path: '/filter-rules',
        title: 'Filter Rules',
        icon: '',
        class: '',
        label: '',
        labelClass: '',
        extralink: false,
        submenu: []
      }
    ]
    },
    {
      path: '',
      title: 'Subscription',
      icon: 'fa fa-list-alt',
      class: 'has-arrow',
      label: '3',
      labelClass: 'label label-rouded label-themecolor',
      extralink: false,
      submenu: [
        {
          path: '/plan-details',
          title: 'Plan Details',
          icon: '',
          class: '',
          label: '',
          labelClass: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/test-console',
          title: 'Test Console',
          icon: '',
          class: '',
          label: '',
          labelClass: '',
          extralink: false,
          submenu: []
        },
        {
          path: '/billing',
          title: 'Billing',
          icon: '',
          class: '',
          label: '',
          labelClass: '',
          extralink: false,
          submenu: []
        }
      ]
      },
  // {
  //   path: '/support',
  //   title: 'Support',
  //   icon: 'fa fa-question-circle',
  //   class: '',
  //   label: '',
  //   labelClass: '',
  //   extralink: false,
  //   submenu: []
  // },
  {
    path: '/settings',
    title: 'Settings',
    icon: 'fa fa-cog',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  }
];

export const adminRoutes: RouteInfo[]  = [
  {
    id: 1,
    path: "/admin/dashboard",
    title: "Dashboard",
    icon: "fa fa-home",
    class: "",
    label: "",
    labelClass: "",
    extralink: false,
    submenu: []
  },
  {
    id: 2,
    path: "/admin/subscriptions",
    title: "Subscriptions",
    icon: "fa fa-pencil-square-o",
    class: "",
    label: "",
    labelClass: "",
    extralink: false,
    submenu: []
  },
  {
    id: 3,
    path: "/admin/billing",
    title: "Billing",
    icon: "fa fa-list-alt",
    class: "",
    label: "",
    labelClass: "",
    extralink: false,
    submenu: []
  },
  {
    id: 4,
    path: "/admin/inbox",
    title: "Inbox",
    icon: "fa fa-envelope-o",
    class: "",
    label: "",
    labelClass: "",
    extralink: false,
    submenu: []
  },
  {
    id: 5,
    path: "",
    title: "Settings",
    icon: "fa fa-cog",
    class: "has-arrow",
    label: "3",
    labelClass: "label label-rouded label-themecolor",
    extralink: false,
    submenu: [
      {
        id: 1,
        path: "/admin/settings",
        title: "Profile",
        icon: "",
        class: "",
        label: "",
        labelClass: "",
        extralink: false,
        submenu: []
      },
      {
        id: 2,
        path: "/admin/plans",
        title: "Plans",
        icon: "",
        class: "",
        label: "",
        labelClass: "",
        extralink: false,
        submenu: []
      },
      {
        id: 3,
        path: "/admin/plan-price",
        title: "Plan Price",
        icon: "",
        class: "",
        label: "",
        labelClass: "",
        extralink: false,
        submenu: []
      }
    ]
  }
];