export const environment = {
  production: true,
  api_url: 'https://535867e31a0d.ngrok.io/',
  websiteApiUrl: 'https://portfolio.wallstreetdrive.com/',
  recaptchaSitekey: "6LfuzbsZAAAAAEAufMQvjwvzez4zARg9LYzew6fR",
  googleAuthProviderID: '756176386875-bb12920gi7lvd7fe7fs51856988f2cmn.apps.googleusercontent.com',
  facebookAuthProviderID: '',
  placeHolderImage: 'https://via.placeholder.com/370x300.png',
  setMinutesForRefreshToken: 60
};
